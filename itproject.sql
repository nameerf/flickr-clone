-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 01, 2012 at 10:10 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `itproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `user` varchar(30) NOT NULL,
  `photoid` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  KEY `uid` (`user`,`photoid`),
  KEY `photoid` (`photoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `comments`:
--   `photoid`
--       `photos` -> `pid`
--

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`user`, `photoid`, `comment`) VALUES
('panos', 13, 'Σχόλιο 1'),
('panos', 11, 'σχόλιο 2....\r\n........'),
('panos', 12, 'άλλο ένα σχόλιο..'),
('giannis', 13, 'ena sxolio se greeklish... :)'),
('giannis', 12, 'kai allo ena sxolio se greeklish...'),
('giannis', 22, 'KDE rocks!!!'),
('giannis', 19, 'keep walking... haha'),
('giannis', 13, 'η πιο δημοφιλης φωτογραφία (μέχρι τώρα)..'),
('giannis', 16, 'nice wallpaper'),
('maria', 31, 'ένα κόκκινο μήλο σε πράσινο φόντο..'),
('panos', 31, 'another comment...in english this time ;)');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `path` varchar(150) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `size` int(11) NOT NULL,
  `width` smallint(4) NOT NULL,
  `height` smallint(4) NOT NULL,
  `privateness` enum('private','public') NOT NULL DEFAULT 'private',
  `address` varchar(150) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `path` (`path`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`pid`, `uid`, `path`, `title`, `description`, `size`, `width`, `height`, `privateness`, `address`, `latitude`, `longitude`, `views`) VALUES
(1, 1, 'photos/panos/1.jpg', 'title 1', 'description 1', 271262, 1280, 800, 'public', '10+Πανεπιστημίου+Πάτρα', 38.256119, 21.746771, 0),
(3, 1, 'photos/panos/3.jpg', 'title 3', 'description 3', 217890, 1280, 800, 'public', '30+Πανεπιστημίου+Πάτρα', 38.257084, 21.748062, 0),
(7, 1, 'photos/panos/7.jpg', 'title 7', 'description 7', 735362, 1280, 800, 'public', '70+Πανεπιστημίου+Πάτρα', 38.258572, 21.750229, 0),
(11, 1, 'photos/panos/11.jpg', 'title 11', 'description 11', 896055, 1280, 800, 'public', '10+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 2),
(12, 1, 'photos/panos/12.jpg', 'title 12', 'description 12', 578331, 1280, 808, 'public', '20+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 4),
(13, 1, 'photos/panos/13.jpg', 'title 13', 'description 13', 1030122, 1280, 800, 'public', '30+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 7),
(14, 1, 'photos/panos/14.jpg', 'title 14', 'description 14', 990053, 1280, 800, 'public', '40+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 0),
(16, 1, 'photos/panos/16.jpg', 'title 16', 'description 16', 711392, 1280, 800, 'public', '60+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 2),
(18, 1, 'photos/panos/18.jpg', 'title 18', 'description 18', 723229, 1280, 800, 'public', '80+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 0),
(19, 1, 'photos/panos/19.jpg', 'title 19', 'description 19', 348902, 1280, 800, 'public', '90+Πανεπιστημίου+Αθήνα', 37.980408, 23.732994, 2),
(21, 2, 'photos/giannis/01514photoshopgeek2560x1600.jpg', 'title 1', 'description 1', 646422, 1280, 800, 'public', '10+Πανεπιστημίου+Πάτρα', 38.256119, 21.746771, 0),
(22, 2, 'photos/giannis/15543_WP_KDEREFLECTION_1280.jpg', 'title 3', 'description 3', 310334, 1280, 800, 'public', '30+Πανεπιστημίου+Πάτρα', 38.257084, 21.748062, 5),
(24, 2, 'photos/giannis/Binary.jpg', 'title 5', 'description 5', 583635, 1280, 800, 'public', '50+Πανεπιστημίου+Πάτρα', 38.257820, 21.748428, 0),
(28, 2, 'photos/giannis/Schaltplan_III.jpg', 'title 9', 'description 9', 562051, 1280, 800, 'public', '90+Πανεπιστημίου+Πάτρα', 38.258392, 21.749788, 0),
(29, 2, 'photos/giannis/UNLIMITED.jpg', 'title 10', 'description 10', 176435, 1280, 800, 'public', '100+Πανεπιστημίου+Πάτρα', 38.258308, 21.749565, 1),
(31, 3, 'photos/maria/001 -  HD Widescreen Wallpapers (235).jpg', 'Το μήλο', 'ένα ροδοκόκκινο μήλο', 278664, 1280, 800, 'public', '44+Κολοκοτρώνη+Αθήνα', 37.977737, 23.729555, 4);

-- --------------------------------------------------------

--
-- Table structure for table `quota`
--

CREATE TABLE IF NOT EXISTS `quota` (
  `uid` int(11) NOT NULL,
  `_usage` int(8) NOT NULL DEFAULT '0',
  KEY `uid` (`uid`,`_usage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quota`
--

INSERT INTO `quota` (`uid`, `_usage`) VALUES
(1, 6502598),
(2, 2278877),
(3, 278664);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag` varchar(30) NOT NULL,
  `photoid` int(11) NOT NULL,
  KEY `tag` (`tag`,`photoid`),
  KEY `photoid` (`photoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- RELATIONS FOR TABLE `tags`:
--   `photoid`
--       `photos` -> `pid`
--

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag`, `photoid`) VALUES
('Desktopography', 7),
('Desktopography', 11),
('Desktopography', 12),
('Desktopography', 13),
('Desktopography', 14),
('Desktopography', 16),
('tag 1', 1),
('tag 1', 3),
('tag 1', 7),
('tag 1', 11),
('tag 1', 12),
('tag 1', 13),
('tag 1', 14),
('tag 1', 16),
('tag 1', 18),
('tag 1', 19),
('tag 1', 21),
('tag 1', 22),
('tag 1', 24),
('tag 1', 28),
('tag 1', 29),
('tag 2', 1),
('tag 2', 3),
('tag 2', 7),
('tag 2', 11),
('tag 2', 12),
('tag 2', 13),
('tag 2', 14),
('tag 2', 16),
('tag 2', 18),
('tag 2', 19),
('tag 2', 21),
('tag 2', 22),
('tag 2', 24),
('tag 2', 28),
('tag 2', 29),
('tag 3', 1),
('tag 3', 3),
('tag 3', 7),
('tag 3', 11),
('tag 3', 12),
('tag 3', 13),
('tag 3', 14),
('tag 3', 16),
('tag 3', 18),
('tag 3', 19),
('tag 3', 21),
('tag 3', 22),
('tag 3', 24),
('tag 3', 28),
('tag 3', 29),
('tag 4', 1),
('tag 4', 3),
('tag 4', 7),
('tag 4', 11),
('tag 4', 12),
('tag 4', 13),
('tag 4', 14),
('tag 4', 16),
('tag 4', 18),
('tag 4', 19),
('tag 4', 21),
('tag 4', 22),
('tag 4', 24),
('tag 4', 28),
('tag 4', 29),
('Άρης', 12),
('καμήλες', 19),
('καρδιές', 1),
('κόκκινο', 1),
('κόκκινο', 3),
('κόκκινο', 18),
('κόκκινο', 31),
('κόκκινος', 12),
('λουλούδι', 3),
('λουλούδι', 18),
('μήλο', 31),
('μπλε', 1),
('ουρανός', 19),
('πλανήτης', 12),
('πουλιά', 14),
('πράσινο', 7),
('πράσινο', 13),
('πράσινο', 14),
('πράσινο', 16),
('ρινόκερος', 16),
('σπίτι', 7),
('τίγρης', 13);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `email`) VALUES
(1, 'panos', 'd05cbf8f2ffebe0cf6daa3abd9d896bc6d5e0a30', 'panos@home.gr'),
(2, 'giannis', 'fa23866901ca2b285b61f5f610b02a3f2deb82ca', 'giannnis@home.gr'),
(3, 'maria', 'e21fc56c1a272b630e0d1439079d0598cf8b8329', 'maria@home.gr');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`photoid`) REFERENCES `photos` (`pid`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`photoid`) REFERENCES `photos` (`pid`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
