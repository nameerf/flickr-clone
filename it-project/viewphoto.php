<?php
header('Content-Type: text/plain');
$pid = isset($_GET['photo_id']) ? $_GET['photo_id'] : NULL;
$size = isset($_GET['size']) ? $_GET['size'] : NULL;

try {
    require 'config.php';
    require 'application/Model.php';
    require 'application/Database.php';
    require 'models/photo_model.php';
    
    $ph_model = new PhotoModel();
    $returned_data = $ph_model->fetchFromPhoto($pid, "path");
    $photo_path = $returned_data['path'];
    if($size != NULL) {
        //switch size sm th sq
        $position = strrpos($photo_path, "/");
        $newstring = substr_replace($photo_path, "small/sm_", $position+1, 0);
        $photo_path = $newstring;
    }
}
catch(Exception $e) {
    $e->getMessage();
}

readfile($photo_path);
exit;
?>
