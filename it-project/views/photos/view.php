<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <div id="photo_view_panel">
            <div id="main_photo_container">
                <a href="<?php echo BASE_URL.'photos/viewfullsize?pid='.$this->photo_id; ?>">
                <?php
                $left_space = 0;
                $ratio = $this->photo_width / $this->photo_height;
                if ($ratio > 1) {
                    if ($this->photo_width > 640) {
                        $view_width = 640;
                        $view_height = 640 / $ratio;
                    }
                    else {
                        $view_width = $this->photo_width;
                        $view_height = $this->photo_height;
                    }
                }
                else {
                    if ($this->photo_height > 640) {
                        $view_width = 640 * $ratio;
                        $view_height = 640;
                        $left_space = (640-$view_width) / 2;
                    }
                    else {
                        $view_width = $this->photo_width;
                        $view_height = $this->photo_height;
                    }
                }
                echo '<img src="'.BASE_URL.'viewphoto.php?photo_id='.$this->photo_id.'" alt="photo" width="'.$view_width.'" height="'.$view_height.'" style="left:'.$left_space.'px;" />';
                ?>
                </a>
            </div>
            <div id="photo_info">
                <h1><?php echo nl2br($this->photo_title); ?></h1>
                <p><?php echo nl2br($this->photo_desc); ?></p>
            </div>
            <div id="comments_container">
                <h2><a id="comments">Σχόλια χρηστών</a></h2>
                <ol id="users_comments">
                    <?php
                    for ($i=0; $i<count($this->comments); $i++) { 
                        echo '<li class="comment_block">';
                            echo '<p class="user">'.$this->comments[$i]['user'].'</p>';
                            echo '<p>'.nl2br($this->comments[$i]['comment']).'</p>'; 
                        echo '</li>';
                    }
                    ?>
                </ol>
                <?php
                if (Session::get('logged_in'))
                {
                ?>
                    <form id="comment_form" action="<?php echo BASE_URL; ?>photos/addcomment" method="post">
                        <input type="hidden" name="current_url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
                        <input type="hidden" name="photo_id" value="<?php echo $this->photo_id ?>" />
                        <textarea name="comment" rows="10" cols="50" wrap="hard" placeholder="Γράψτε το σχόλιό σας εδώ..."></textarea>
                        <br /><br />
                        <input class="button" type="submit" value="Υποβολή σχολίου" />
                    </form>
                <?php
                }
                ?>
            </div>
        </div>
        
        <!-- Side bar -->
        <div id="photo_info_sidebar">
            <div class="slight_border_bottom temp">
                <p>Ανέβηκε από τον/την χρήστη <span class="important site_blue"><?php echo $this->photo_owner; ?></span></p>
            </div>

            <div id="mapholder_view" style="padding:0;">
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                <script type="text/javascript" src="<?php echo BASE_URL; ?>controllers/js/map.js"></script>
                <script type="text/javascript">
                    var photo_address = '<?php echo $this->photo_address; ?>';
                    var photo_address_parts = photo_address.split("+");
                    photo_address = photo_address_parts[1]+" "+photo_address_parts[0]+", "+photo_address_parts[2];
                    var photo_lat = '<?php echo $this->photo_lat; ?>';
                    var photo_lon = '<?php echo $this->photo_lon; ?>';
                    showPhotoLocation("mapholder_view", photo_address, photo_lat, photo_lon);
                </script>
            </div>

            <div class="slight_border_bottom temp">
                <p>
                    <span style="margin:0 30px"><?php echo $this->photo_views ?> προβολές</span>
                    <a style="margin:0 30px" href="#comments"><?php echo count($this->comments) ?> σχόλια</a>
                </p>
            </div>
            
            <div id="photo_tags" class="temp">
                <h4 class="slight_border_bottom">Ετικέτες</h4>
                <p>
                <?php
                echo '<a href="'.BASE_URL.'search?q='.$this->photo_tags[0]['tag'].'&w=tag">'.$this->photo_tags[0]['tag'].'</a>';
                for ($i=1; $i<count($this->photo_tags); $i++) {
                    $tag = $this->photo_tags[$i]['tag'];
                    echo '&nbsp;&bull;&nbsp;';
                    echo '<a href="'.BASE_URL.'search?q='.$tag.'&w=tag">'.$tag.'</a>';
                }
                ?>
                </p>
            </div>
            
            <?php
            $msg;
            switch ($this->photo_priv) {
                case "private":
                    $msg = "Αυτή η φωτογραφία είναι εμφανίσιμη μόνο στον χρήστη ".$this->photo_owner;
                    break;
                case "public":
                    $msg = "Αυτή η φωτογραφία είναι εμφανίσιμη σε όλους";
                    break;
            }  
            ?>
            <div class="temp">
                <h4 class="slight_border_bottom">Ιδιωτικότητα</h4>
                <p><?php echo $msg ?></p>
            </div>
            
        </div>
    </div>
    <div id="content_bottom" style="clear:both;"></div>
</div>
