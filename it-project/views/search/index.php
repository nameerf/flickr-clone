<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Αναζήτηση φωτογραφιών</h1>
        <hr class="space" />
        <br />
        <p class="toggle">
        <?php
        $search_with = "";
        if (isset($_GET['w']) && ($_GET['w'] == "tags")) {
            echo '<a href="'.BASE_URL.'search">Πλήρες κείμενο</a>';
            echo '&nbsp;|&nbsp;';
            echo '<span class="selected">Ετικέτες μόνο</span>';
            $search_with = "tags";
        }
        else {
            echo '<span class="selected">Πλήρες κείμενο</span>';
            echo '&nbsp;|&nbsp;';
            echo '<a href="?w=tags">Ετικέτες μόνο</a>';
        }
        ?>
        </p>
        <div id="search">
            <form action="<?php echo BASE_URL ?>search" method="get">
            <?php
            if ($search_with == "tags")
            {
            ?>
                <input name="q" type="text" autocomplete="off" onkeyup="showSuggestions(this.value)" onblur="setTimeout(function(){hideSuggestions(1);},100);" />
                <ul id="suggestions1" class="hint_box">
                <?php
                for ($i=0; $i<MAX_TAG_SUGGESTIONS; $i++)
                {
                ?> 
                   <li onclick="selectTag(1, this.textContent)">&nbsp;</li>
                <?php
                }
                ?>
                </ul>
                <input type="hidden" name="w" value="<?php echo $search_with; ?>" />
            <?php
            }
            else
            {
            ?>
                <input name="q" type="text" autocomplete="off" />
            <?php    
            }
            ?>
                <input class="button" type="submit" value="Αναζήτηση" />
            </form>
        </div>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>controllers/js/autocomplete.js"></script>
    </div>
    <div id="content_bottom">
        <h2>Αναζήτηση μέσω δημοφιλών ετικετών</h2>
        <div class="tag_cloud">
        <?php
        $num_of_distinct_tags = count($this->tags);
        $tag_box_limit = ($num_of_distinct_tags > MAX_TAGS_OF_TAGCLOUD) ? MAX_TAGS_OF_TAGCLOUD : $num_of_distinct_tags;
        for ($i=0; $i<$tag_box_limit; $i++) {
            $tag = $this->tags[$i];
            $frequency = $this->tag_freq[$i];
            // the line below gives a font size from 75% to 300%
            $weight = 150 * (1.0 + (1.5*$frequency - $this->max_freq/2) / $this->max_freq);
            echo '<a href="'.BASE_URL.'search?q='.$tag.'&w=tag" style="font-size:'.$weight.'%;">'.$tag.'</a>';
        }
        ?>
        </div>
    </div>
</div>