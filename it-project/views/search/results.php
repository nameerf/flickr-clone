<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <?php
        $type_of = "";
        if ($this->search_with == "tag") {
            $type_of = "την ετικέτα";
        } else if ($this->search_with == "tags") {
            $type_of = "την ετικέτα";
        } else {
            $type_of = "τις λέξεις";
        }
        echo '<h1>Φωτογραφίες που περιέχουν '.$type_of.' <span style="color:#FF0066; font-weight:bold;">'.$this->search_query.'</span></h1>';
        ?>
        <hr class="space" />

        <br />
        <p class="toggle" style="width:50%; margin:0 auto; padding-bottom:3px;">
        <?php
        $search_with = "";
        if (isset($_GET['w']) && ($_GET['w'] == "tags")) {
            if(isset($_GET['q'])) {
                echo '<a href="?q='.$_GET['q'].'">Πλήρες κείμενο</a>';
            }
            else {
                echo '<a href="">Πλήρες κείμενο</a>';
            }
            echo '&nbsp;|&nbsp;';
            echo '<span class="selected">Ετικέτες μόνο</span>';
            $search_with = "tags";
        }
        else {
            echo '<span class="selected">Πλήρες κείμενο</span>';
            echo '&nbsp;|&nbsp;';
            if(isset($_GET['q'])) {
                echo '<a href="?q='.$_GET['q'].'&w=tags">Ετικέτες μόνο</a>';
            }
            else {
                echo '<a href="?w=tags">Ετικέτες μόνο</a>';
            }
        }
        ?>
        </p>
        <div id="search">
            <form action="<?php echo BASE_URL ?>search" method="get">
            <?php
            if ($search_with == "tags")
            {
            ?>
                <input name="q" type="text" autocomplete="off" onkeyup="showSuggestions(this.value)" onblur="setTimeout(function(){hideSuggestions(1);},100);" />
                <ul id="suggestions1" class="hint_box">
                <?php
                for ($i=0; $i<MAX_TAG_SUGGESTIONS; $i++)
                {
                ?> 
                   <li onclick="selectTag(1, this.textContent)">&nbsp;</li>
                <?php
                }
                ?>
                </ul>
                <input type="hidden" name="w" value="<?php echo $search_with; ?>" />
            <?php
            }
            else
            {
            ?>
                <input name="q" type="text" autocomplete="off" />
            <?php    
            }
            ?>
                <input class="button" type="submit" value="Αναζήτηση" />
            </form>
        </div>
        <script type="text/javascript" src="<?php echo BASE_URL; ?>controllers/js/autocomplete.js"></script>
        <br />
        <br />
        
        <?php
        $num_of_results = count($this->photos_pids);
        $num_of_rows = floor($num_of_results/3);
        $last_cells = $num_of_results%3;
        
        if ($num_of_results >= 3) {
            $cells_per_row = 3;
            echo '<table class="nine_popular">';
        }
        else if ($num_of_results == 2) {
            $cells_per_row = 2;
            echo '<table class="nine_popular" style="width:66%; margin:0 auto;">';
        }
        else {
            $cells_per_row = 1;
            echo '<table class="nine_popular" style="width:33%; margin:0 auto;">';
        }
        $i = 0;
        for ($k=0; $k<$num_of_rows; $k++)
        {
        ?>
            <tr valign="bottom">
            <?php
            for ($j=0; $j<$cells_per_row; $j++)
            {
            ?>
                <td class="photo">
                    <span class="photo_container">
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL.$this->photos_small_urls[$i]; ?>" />
                        </a>
                    </span>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
            <tr valign="top">
            <?php
            $i -= $cells_per_row;
            for ($j=0; $j<$cells_per_row; $j++)
            {
            ?>
                <td class="owner">
                    <p>
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <?php echo wordwrap($this->photos_titles[$i],30,"<br />\n", true); ?>
                        </a>
                        <br />
                        <span style="font-size:small;">Από </span>
                        <a style="font-style:normal; font-size:small;"><?php echo $this->photos_owners[$i]; ?></a>
                    </p>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
        <?php
        }
        ?>
            <tr valign="bottom">
            <?php
            for ($j=0; $j<$last_cells; $j++)
            {
            ?>
                <td class="photo">
                    <span class="photo_container">
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL.$this->photos_small_urls[$i]; ?>" />
                        </a>
                    </span>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
            <tr valign="top">
            <?php
            $i -= $last_cells;
            for ($j=0; $j<$last_cells; $j++)
            {
            ?>
                <td class="owner">
                    <p>
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <?php echo wordwrap($this->photos_titles[$i],30,"<br />\n", true); ?>
                        </a>
                        <br />
                        <span style="font-size:small;">Από </span>
                        <a style="font-style:normal; font-size:small;"><?php echo $this->photos_owners[$i]; ?></a>
                    </p>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
        </table>
    </div>
    <div id="content_bottom" style="clear:left; width:70%;">
    <?php
    if ($this->num_of_matches == 0)
    {
    ?>
        <p>Δεν βρέθηκε κάποιο αποτέλεσμα που να ανταποκρίνεται στην αναζήτησή σας</p>
        <br />
        <p>Μερικές προτάσεις για να κάνετε αποτελεσματικότερη την αναζήτησή σας:</p>
        <br />
        <ul>
            <li>Ελέγξτε την γραμματική σας.</li>
            <li>Δοκιμάστε πιο γενικού περιεχομένου λέξεις.</li>
            <li>Δοκιμάστε διαφορετικές λέξεις που έχουν την ίδια σημασία με αυτές της αναζήτησή σας.</li>
        </ul>
    <?php
    }
    else
    {
    ?>
        <div id="page_navigator">
            <div class="paginator">
            <?php
            if ($this->current_page == 1) {
                $class = "disabled";
                $prev_page = "";
                $first_page = "";
            }
            else {
                $class = "";
                $prev_page = BASE_URL."search?q=".$this->search_query."&w=".$this->search_with."&page=".($this->current_page-1);
                $first_page = BASE_URL."search?q=".$this->search_query."&w=".$this->search_with."&page=".(1);
            }
            ?>
                <a class="<?php echo $class; ?>" href="<?php echo $first_page; ?>">&llarr; πρώτη</a>
                <a class="<?php echo $class; ?>" style="margin-right:20px;" href="<?php echo $prev_page; ?>">&larr; προηγούμενη</a>
                <span class="pages">
                <?php
                for ($i=0; $i<($this->num_of_matches/MAX_PHOTOS_PER_PAGE) && $i<5; $i++)
                {
                    $class = "";
                    if (($i+1) == $this->current_page) {
                        $class = "selected_page";
                    }
                    echo '<a class="'.$class.'" href="'.BASE_URL.'search?q='.$this->search_query."&w=".$this->search_with.'&page='.($i+1).'">'.($i+1).'</a>';
                }
                ?>
                </span>
            <?php
            if ($this->current_page == ceil($this->num_of_matches/MAX_PHOTOS_PER_PAGE)) {
                $class = "disabled";
                $next_page = "";
                $last_page = "";
                $end_results = $this->num_of_matches;
            }
            else {
                $class = "";
                $next_page = BASE_URL."search?q=".$this->search_query."&w=".$this->search_with."&page=".($this->current_page+1);
                $last_page = BASE_URL."search?q=".$this->search_query."&w=".$this->search_with."&page=".(ceil($this->num_of_matches/MAX_PHOTOS_PER_PAGE));
                $end_results = $this->current_page*MAX_PHOTOS_PER_PAGE;
            }
            ?>
                <a class="<?php echo $class; ?>" style="margin-left:20px;" href="<?php echo $next_page; ?>">επόμενη &rarr;</a>
                <a class="<?php echo $class; ?>" href="<?php echo $last_page; ?>">τελευταία &rrarr;</a>
            </div>
            <div class="results">
                (εμφανίζονται <?php echo ($this->current_page*MAX_PHOTOS_PER_PAGE)-MAX_PHOTOS_PER_PAGE+1 . " έως " . $end_results . " από " . $this->num_of_matches; ?> αποτελέσματα)
            </div>
        </div>
    <?php
    }
    ?>
    </div>
</div>