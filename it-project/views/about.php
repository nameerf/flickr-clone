<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content" style="width:80%;">
        <h1>Πληροφορίες σχετικά με τo freePhotoSharing</h1>
        <hr />
        <p>
            Το σύστημα freePhotoSharing αναπτύχθηκε στα πλαίσια του μαθήματος "Τεχνολογίες Διαδυκτίου"
            που διδάσκεται στο τμήμα "Μηχανικών Ηλεκτρονικών Υπολογιστών και Πληροφορικής" του
            Πανεπιστημίου Πατρών.
        </p>
        <br /><br />
        <p>
            Η κύρια υπηρεσία του συστήματος είναι ο online διαμοιρασμός φωτογραφιών.
        </p>
        <br /><br />
        <p>
            Σχεδιασμός και υλοποίηση συστήματος: Γιαννακόπουλος Παναγιώτης (giannakp).
        </p>
    </div>
    <div id="content_bottom" style="width:80%;"></div>
</div>