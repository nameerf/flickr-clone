<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Τροποποίηση στοιχείων φωτογραφίας</h1>
        <hr />
        <br /><br />
        <form id="upload_photo" action="<?php echo BASE_URL ;?>userarea/updatephoto" autocomplete="off" onsubmit="return validateUserInput(this)" method="post">
            <input type="hidden" name="photo_id" value="<?php echo $this->photo_id ?>" />
            <input type="hidden" name="uploaded_image" value="edit" />
            <div style="text-align:center;">
                <?php echo '<img src="'.BASE_URL.$this->small_photo_url.'" style="text-align:center; border:1px solid #FFFFFF; box-shadow:0 0 5px #888888;" />'; ?>
            </div>
            <br /><br /><br />
            <div>
                <label for="title_id">* Τίτλος (μέχρι xxx χαρακτήρες)</label>
                <input type="text" id="title_id" name="title" <?php echo "value='$this->photo_title'"; ?> />
                <br /><br />
                <label for="description_id">* Περιγραφή (μέχρι xxx χαρακτήρες)</label>
                <textarea id="description_id" name="description" rows="9" cols="45"><?php echo $this->photo_desc; ?></textarea>
                <br /><br />
                <div>
                    <label for="tag1">* Ετικέτες (μέχρι 10)</label>
                    <div id="tags">
                        <?php
                        $num_of_tags = count($this->photo_tags);
                        for ($i=0; $i<$num_of_tags; $i++) {
                            echo '<div>';
                                echo '<input id="tag'.($i+1).'_id" class="tag_textfield" name="tag'.($i+1).'" type="text" value="'.$this->photo_tags[$i]['tag'].'" onkeyup="showSuggestions(this.value)" onblur="setTimeout(function(){hideSuggestions('.($i+1).');},100);" />';
                                echo '<ul id="suggestions'.($i+1).'" class="hint_box">';
                                for ($j=0; $j<MAX_TAG_SUGGESTIONS; $j++)
                                {
                                    echo '<li onclick="selectTag('.($i+1).', this.textContent)">&nbsp;</li>';
                                }
                                echo '</ul>';
                            echo '</div>';
                        }
                        $disabled = 'disabled="disabled"';
                        if ($num_of_tags > 1) {
                            $disabled = "";
                        }
                        ?>
                    </div>
                    <input type="button" id="add_tag" value="+" onclick="addTag()" />
                    <input type="button" id="remove_tag" value="-" <?php echo $disabled; ?> onclick="removeTag()" />
                </div>
                <br style="clear:left;" /><br />
                <label for="privateness_id">* Ιδιωτικότητα</label>
                        <select id="privateness_id" name="privateness">
                            <?php
                            $selected1 = $selected2 = "";
                            if ($this->photo_priv == "private") {
                                $selected1 = 'selected="selected"';
                            }
                            else if ($this->photo_priv == "public") {
                                $selected2 = 'selected="selected"';
                            }
                            ?>
                            <option value="private" <?php echo $selected1; ?>>Ιδιωτική φωτογραφία</option>
                            <option value="public" <?php echo $selected2; ?>>Δημόσια φωτογραφία</option>
                        </select>
                <br /><br />
                <div>
                    <label for="street_number">&nbsp; Τοποθεσία αναφοράς <br /> (οδός - αριθμός - πόλη)</label>
                    <input type="text" name="street" size="19" value="<?php echo $this->address_parts[1] ?>" />
                    <input id="street_number" name="street_number" type="text" size="4" value="<?php echo $this->address_parts[0] ?>" />
                    <input type="text" name="city" size="14" value="<?php echo $this->address_parts[2] ?>" />
                </div>
                <br /><br />
            </div>
            <span>
                <input class="button" type="reset" value="Καθαρισμός" />
                <input class="button" type="submit" value="Αποθήκευση" />
            </span>
        </form>
        <script type="text/javascript" src="<?php echo BASE_URL;?>views/userarea/js/addremovetags.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/autocomplete.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/upload.js"></script>
    </div>
    <div id="content_bottom"></div>
</div>