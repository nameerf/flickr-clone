function addTag()
{
	var tags_div = document.getElementById("tags");
	var num_of_tags = tags_div.children.length;
	
	num_of_tags++;
	
	var new_div_container = document.createElement('div');
	
	var new_tag = document.createElement('input');
	new_tag.type = 'text';
	new_tag.id = 'tag'+num_of_tags+'_id';
	new_tag.name = 'tag'+num_of_tags;
	new_tag.autocomplete = 'off';
	new_tag.setAttribute('class', 'tag_textfield');
	new_tag.setAttribute('onkeyup', 'showSuggestions(this.value)');
	new_tag.setAttribute('onblur', 'setTimeout(function(){hideSuggestions('+num_of_tags+');},100);');
	
	var new_ul = document.createElement('ul');
	new_ul.setAttribute('id', 'suggestions'+num_of_tags);
	new_ul.setAttribute('class', 'hint_box');
	
	for (var i=0; i<5; i++) {
	    var new_li = document.createElement('li');
	    new_li.textContent = "&nbsp;";
	    new_li.setAttribute('onclick', 'selectTag('+num_of_tags+', this.textContent)');
	    new_ul.appendChild(new_li);
	}
	
	new_div_container.appendChild(new_tag);
	new_div_container.appendChild(new_ul);
	tags_div.appendChild(new_div_container);
	
	document.getElementById("remove_tag").disabled = false;
	
	if (num_of_tags == 10)
		document.getElementById("add_tag").disabled = true;
}

function removeTag()
{
	var tags_div = document.getElementById("tags");
	var num_of_tags = tags_div.children.length;
	
	tags_div.removeChild(tags_div.children[num_of_tags-1]);
	
	num_of_tags--;
	
	if (num_of_tags == 1)
		document.getElementById("remove_tag").disabled = true;
	
	if (num_of_tags == 9 || num_of_tags == 1)
		document.getElementById("add_tag").disabled = false;
}
