<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Προσωπικό άλμπουμ</h1>
        <hr />
        <br />
        <div class="page_description">
            <p>
                Καλώς ήρθατε στο προσωπικό σας άλμπουμ! Από εδώ μπορείτε να διαχειριστείτε τις
                φωτογραφίες που έχετε ανεβάσει στο σύστημα. Για κάθε φωτογραφία μπορείτε να:
            </p>
            <ol>
                <li>την προβάλετε και να δείτε όλες τις πληροφορίες σχετικά με αυτήν..</li>
                <li>τροποποιήσετε τα στοιχεία της (τον τίτλο, την περιγραφή της κλπ.)..</li>
                <li>την διαγράψετε οριστικά από το σύστημα..</li>
            </ol>
            <p>..πατώντας στο αντίστοιχο κουμπάκι που βρίσκεται μέσα στο πλαίσιο της κάθε φωτογραφίας.</p>
        </div>
        <br /><br />
        <div id="user_photos_container">
            <ul>
            <?php
            for ($i=0; $i<count($this->photos_urls); $i++)
            {
            ?>
                <li class="photo">
                    <a class="img_container" href="<?php echo BASE_URL.'photos/viewfullsize?pid='.$this->photos_pids[$i]; ?>">
                        <img src="<?php echo BASE_URL.$this->thumbs_urls[$i]; ?>" />
                    </a>
                    <div>
                        <a href="<?php echo BASE_URL.'photos/view?pid='.$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL; ?>public/images/view.png" alt="Προβολή" title="Προβολή φωτογραφίας" />
                        </a>
                        <a href="<?php echo BASE_URL.'userarea/editphoto?pid='.$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL; ?>public/images/edit.png" alt="Τροποποίηση" title="Τροποποίηση στοιχείων φωτογραφίας" />
                        </a>
                        <a href="<?php echo BASE_URL.'userarea/deletephoto?pid='.$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL; ?>public/images/delete.png" alt="Διαγραφή" title="Διαγραφή φωτογραφίας" onclick="return confirmDelete()" />
                        </a>
                    </div>
                </li>
            <?php
            }
            ?>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
    function confirmDelete() {  
        return confirm("Είστε σίγουρος-η πως θέλετε να διαγράψετε τη φωτογραφία;");
    }
    </script>
    <div id="content_bottom"></div>
</div>
