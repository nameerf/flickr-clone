<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content" style="width:80%;">
        <h1>Περιοχή μελών</h1>
        <hr class="space" />
        <p>
            Από τη σελίδα αυτή μπορείτε να:
            <br /><br /><br />
            <ul class="unordered_list">
                <li><a href="<?php echo BASE_URL; ?>userarea/album">Δείτε το προσωπικό άλμπουμ με τις φωτογραφίες σας και να τις διαχειριστείτε</a></li>
                <li><a href="<?php echo BASE_URL; ?>userarea/uploadphoto">Ανεβάσετε μια φωτογραφία στο προσωπικό σας άλμπουμ</a></li>
                <li><a href="<?php echo BASE_URL; ?>userarea/uploadphotos">Ανεβάσετε μαζικά φωτογραφίες (μέσω συμπιεσμένου αρχείου) στο προσωπικό σας άλμπουμ</a></li>
                <li><a href="<?php echo BASE_URL; ?>userarea/editaccount">Τροποποιήσετε τα στοιχεία του λογαριασμού σας</a></li>
            </ul>
        </p>
    </div>
    <div id="content_bottom" style="width:80%;"></div>
</div>