<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Μαζικό ανέβασμα φωτογραφιών</h1>
        <hr class="space" />
        <p>
            Από τη σελίδα αυτή μπορείτε να ανεβάσετε μαζικά φωτογραφίες στο προσωπικό σας άλμπουμ
            μέσω ενός συμπιεσμένου αρχείου zip. Τα στοιχεία των φωτογραφιών (τίτλος, περιγραφή, κλπ.)
            θα πρέπει να γραφούν (σε κατάλληλη μορφή xml) σε ένα αρχείο με όνομα info.xml. Για 
            διευκόλυνσή σας, στον παρακάτω σύνδεσμο υπάρχει ένα πρότυπο αρχείο xml το οποίο μπορείτε
            να κατεβάσετε και να τροποποιήσετε ανάλογα.
        </p>
        <br />
        <p>
            Κατεβάστε 
            <a href="<?php echo BASE_URL."info.xml"; ?>" target="_blank">εδώ</a>
            το πρότυπο αρχείο info.xml (κάντε δεξί κλίκ και "αποθήκευση συνδέσμου ως")
        </p>
        <br />
        <p>
            Για τις φωτογραφίες που θα ανεβάσετε ισχύουν οι εξής περιορισμοί:
            <br /><br />
            <ol>
                <li>Οι επιτρεπτές μορφές είναι μόνο οι PNG και JPEG.</li>
                <li>Η μέγιστη ανάλυση που επιτρέπεται είναι 1280x1024 ή αντίστοιχα 1024x1280 pixels. 
                    Σε περίπτωση μεγαλύτερης ανάλυσης, η φωτογραφία θα συρικνωθεί αυτόματα
                    διατηρώντας την αναλογία των διαστάσεών της.</li>
                <li>Το συνολικό μέγεθος των φωτογραφιών δεν πρέπει να ξεπερνά τα 50 MB, που είναι το
                    μέγιστο μέγεθος του αποθηκευτικού σας χώρου. Σε αντίθετη περίπτωση θα ανέβουν στο
                    άλμπουμ σας μόνο κάποιες από τις φωτογραφίες (από τις πρώτες με βάση τη σειρά που
                     εμφανίζονται στο αρχείο info.xml) μέχρι να συμπληρωθεί ο αποθηκευτικός σας χώρος.</li>
                <li>Τέλος το μέγεθος κάθε φωτογραφίας (μεμονωμένα) δεν πρέπει να ξεπερνά τα 3MB.</li>
            </ol>
        </p>
        <br /><br />
        <form id="upload_photo" action="<?php echo BASE_URL; ?>userarea/uploadandsavephotos" onsubmit="return validateUserInputMassUpload(this)" method="post" enctype="multipart/form-data">
            <p>
                <label for="uploaded_zipped_file">Όνομα συμπιεσμένου αρχείου</label>
                <input type="file" id="uploaded_zipped_file" name="uploaded_zipped_file" size="36" style="width:388px;" />
            </p>
            <br /><br />
            <span><input class="button" type="submit" value="Ανέβασμα" /></span>
        </form>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/upload.js"></script>
        
    </div>
    <div id="content_bottom"></div>
</div>