<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Τροποποίηση λογαριασμού</h1>
        <br />
        <p>Αλλάξτε τον κωδικό σας και/ή το e-mail σας</p>
        <br />
        <div id="edit_account">
            <form action="<?php echo BASE_URL; ?>userarea/updateaccount" onsubmit="return validateUserInput(this)" method="post">
                <br />
                <p>
                    <span><label for="password">Νέος κωδικός πρόσβασης (password)</label></span>
                    <input id="password" name="password" type="password" />
                </p>
                <p>
                    <span><label for="password2">Νέος κωδικός πρόσβασης (επιβεβαίωση)</label></span>
                    <input id="password2" name="password2" type="password" />
                </p>
                <p>
                    <span><label for="email">Νέο e-mail</label></span>
                    <input id="email" name="email" type="text" />
                </p>
                <br />
                <p>
                    <span>&nbsp;</span>
                    <input style="margin-left:88px;" class="button" type="reset" value="Καθαρισμός" />
                    <input class="button" type="submit" value="Αποστολή" />
                </p>
            </form>
        </div>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/editaccount.js"></script>
    </div>
    <div id="content_bottom" style="width:70%;"></div>
</div>
