<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Ανέβασμα φωτογραφίας</h1>
        <hr class="space" />
        <p>
            Από τη σελίδα αυτή μπορείτε να ανεβάσετε μια φωτογραφία στο προσωπικό σας άλμπουμ.
            Για την φωτογραφία που θα ανεβάσετε ισχύουν οι εξής δύο περιορισμοί:
            <br /><br />
            <ol>
                <li>Οι επιτρεπτές μορφές είναι μόνο οι PNG και JPEG.</li>
                <li>Η μέγιστη ανάλυση που επιτρέπεται είναι 1280x1024 ή αντίστοιχα 1024x1280 pixels. 
                    Σε περίπτωση μεγαλύτερης ανάλυσης, η φωτογραφία θα συρικνωθεί αυτόματα
                    διατηρώντας την αναλογία των διαστάσεών της.</li>
                <li>Τέλος το μέγεθος κάθε φωτογραφίας δεν πρέπει να ξεπερνά τα 3MB.</li>
            </ol>
        </p>
        <br /><br />
        <form id="upload_photo" action="<?php echo BASE_URL; ?>userarea/uploadandsavephoto" autocomplete="off" onsubmit="return validateUserInput(this)" method="post" enctype="multipart/form-data">
            <p>
                <label for="image_id">Όνομα αρχείου</label>
                <input type="file" id="image_id" name="uploaded_image" size="36" style="width:388px;" />
            </p>
            <br /><br />
            <div>
                <label for="title_id">* Τίτλος (μέχρι xxx χαρακτήρες)</label>
                <input type="text" id="title_id" name="title" style="width:300px;" />
                <br /><br />
                <label for="description_id">* Περιγραφή (μέχρι xxx χαρακτήρες)</label>
                <textarea id="description_id" name="description" rows="9" cols="45"></textarea>
                <br /><br />
                <div>
                    <label for="tag1">* Ετικέτες (μέχρι 10)</label>
                    <div id="tags">
                        <div>
                            <input class="tag_textfield" id="tag1_id" name="tag1" type="text" onkeyup="showSuggestions(this.value)" onblur="setTimeout(function(){hideSuggestions(1);},100);" />                              
                            <ul id="suggestions1" class="hint_box">
                            <?php
                            for ($i=0; $i<MAX_TAG_SUGGESTIONS; $i++)
                            {
                            ?> 
                               <li onclick="selectTag(1, this.textContent)">&nbsp;</li>
                            <?php
                            }
                            ?>
                            </ul>
                        </div>
                    </div>
                    <input type="button" id="add_tag" value="+" onclick="addTag()" />
                    <input type="button" id="remove_tag" value="-" disabled="true" onclick="removeTag()" />
                </div>
                <br style="clear:left;" /><br />
                <label for="privateness_id">* Ιδιωτικότητα</label>
                    <select id="privateness_id" name="privateness">
                        <option value="private">Ιδιωτική φωτογραφία</option>
                        <option value="public">Δημόσια φωτογραφία</option>
                    </select>
                <br /><br />
                <div>
                    <label for="street_number">&nbsp; Τοποθεσία αναφοράς <br /> (οδός - αριθμός - πόλη)</label>
                    <input type="text" name="street" size="19" placeholder="οδός" />
                    <input id="street_number" name="street_number" type="text" size="4" placeholder="αριθμός" />
                    <input type="text" name="city" size="14" placeholder="πόλη" />
                </div>
                <br /><br />
            </div>
            <span>
                <input class="button" type="reset" value="Καθαρισμός" />
                <input class="button" type="submit" value="Ανέβασμα" />
            </span>
        </form>
        <script type="text/javascript" src="<?php echo BASE_URL;?>views/userarea/js/addremovetags.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/autocomplete.js"></script>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/upload.js"></script>
    </div>
    <div id="content_bottom"></div>
</div>