<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Καλώς ορίσατε</h1>
        <div id="welcome_div">
            <p>Το freePhotoSharing είναι ένα ελληνικό site για τον online διαμοιρασμό φωτογραφιών.</p><br />
            <p>Στα δεξιά εμφανίζεται ο χάρτης ο οποίος παρουσιάζει (σε μικρογραφία) τις δημόσιες φωτογραφίες
               που ανήκουν στα όρια της περιοχής που εμφανίζεται.</p><br />
            <p>Και παρακάτω εμφανίζονται οι 10 δημοφιλέστερες φωτογραφίες, βάσει των προβολών τους.</p><br />
            <div class="most_viewed_photo">
                <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[0]; ?>">
                <?php
                $ratio = $this->photos_widths[0] / $this->photos_heights[0];
                if ($ratio > 1) {
                    if ($this->photos_widths[0] > 340) {
                        $view_width = 340;
                        $view_height = 340 / $ratio;
                    }
                    else {
                        $view_width = $this->photos_widths[0];
                        $view_height = $this->photos_heights[0];
                    }
                }
                else {
                    if ($this->photos_heights[0] > 340) {
                        $view_width = 340 * $ratio;
                        $view_height = 340;
                    }
                    else {
                        $view_width = $this->photos_widths[0];
                        $view_height = $this->photos_heights[0];
                    }
                }
                echo '<img src="'.BASE_URL.'viewphoto.php?photo_id='.$this->photos_pids[0].'" alt="most_pop_photo" width="'.$view_width.'" height="'.$view_height.'" />';
                ?>
                </a>
                <p class="owner">
                    <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[0]; ?>">
                        <?php echo wordwrap($this->photos_titles[0],30,"<br />\n", true); ?>
                    </a>
                    <br />
                    <span style="font-size:small;">Από&nbsp;</span>
                    <a style="font-size:small; font-style:normal;"><?php echo $this->photos_owners[0]; ?></a>
                </p>
            </div>
        </div>
        <div id="mapholder">
            <?php
                $coordinates = addressToCoordinates("Πάτρα");
            
                $lat = $coordinates["latitude"];
                $lng = $coordinates["longitude"];
            ?>
            
            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            <script type="text/javascript" src="<?php echo BASE_URL; ?>controllers/js/map.js"></script>
            <script>
                
                var default_lat = <?php echo $lat; ?>;
                var default_lng = <?php echo $lng; ?>;
                //getUserLocation();
                getUserLocation();
            </script>
        </div>
        <table class="nine_popular">
        <?php
        for ($i=1,$k=0; $k<3; $k++)
        {
        ?>
            <tr valign="bottom">
            <?php
            for ($j=0; $j<3; $j++)
            {
            ?>
                <td class="photo">
                    <span class="photo_container">
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <img src="<?php echo BASE_URL.'viewphoto.php?photo_id='.$this->photos_pids[$i].'&size=sm'; ?>" />
                        </a>
                    </span>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
            <tr valign="top">
            <?php
            $i -= 3;
            for ($j=0; $j<3; $j++)
            {
            ?>
                <td class="owner">
                    <p>
                        <a href="<?php echo BASE_URL."photos/view?pid=".$this->photos_pids[$i]; ?>">
                            <?php echo wordwrap($this->photos_titles[$i],30,"<br />\n", true); ?>
                        </a>
                        <br />
                        <span style="font-size:small;">Από&nbsp;</span>
                        <a style="font-size:small; font-style:normal;"><?php echo $this->photos_owners[$i]; ?></a>
                    </p>
                </td>
                <?php
                $i++;
            }
            ?>
            </tr>
        <?php
        }
        ?>
        </table>
    </div>
    <div id="content_bottom">
        <h2>Αναζήτηση μέσω δημοφιλών ετικετών</h2>
        <div class="tag_cloud">
        <?php
        $num_of_distinct_tags = count($this->tags);
        $tag_box_limit = ($num_of_distinct_tags > MAX_TAGS_OF_TAGCLOUD) ? MAX_TAGS_OF_TAGCLOUD : $num_of_distinct_tags;
        for ($i=0; $i<$tag_box_limit; $i++) {
            $tag = $this->tags[$i];
            $frequency = $this->tag_freq[$i];
            // the line below gives a font size from 75% to 300%
            $weight = 150 * (1.0 + (1.5*$frequency - $this->max_freq/2) / $this->max_freq);
            echo '<a href="'.BASE_URL.'search?q='.$tag.'&w=tag" style="font-size:'.$weight.'%;">'.$tag.'</a>';
        }
        ?>
        </div>
    </div>
</div>