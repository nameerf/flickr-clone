<!DOCTYPE html>
<html lang="el">
    <head>
        <title>free Photo Sharing</title>
        <meta name="description" content="free photo sharing" />
        <meta name="keywords" content="free, photo, share, upload, search, view" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>public/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>public/css/<?php echo $this->style; ?>.css" />
    </head>
    <body>
        <!-- Header -->
        <div id="header">
            <div id="top">
                <div id="logo">
                    <a href="<?php echo BASE_URL; ?>" style="text-decoration:none;">
                        free<span style="color:#3366FF;">Photo<span style="color:#FF0066;">S</span>haring</span>
                    </a>
                </div>
                <div id="navbar">
                    <a href="<?php echo BASE_URL; ?>">Αρχική Σελίδα</a>
                    <a href="<?php echo BASE_URL; ?>about">Σχετικά</a>
                    <div id="search_header_container">
                        <form action="<?php echo BASE_URL ?>search" method="get" autocomplete="off">
                            <input class="navbar_search" type="text" name="q" />
                            <input class="navbar_search_button" type="submit" value="Αναζήτηση" />
                        </form>
                    </div>
                </div>
            </div>
            
            <div id="user_bar_container">
                <div id="user_bar">
                    <div class="user">
                    <?php
                    Session::init();
                    if (Session::get('logged_in'))
                    {
                    ?>
                        <p>
                            <strong>Καλώς ήλθατε</strong>
                            <a class="user_submenu_button" href="<?php echo BASE_URL; ?>userarea" onmouseenter="showusersubmenu()">
                                <span class="user_text"><?php echo Session::get('user'); ?></span>
                                <span class="down_arrow_icon"></span>
                            </a>
                            <a class="logout" href="<?php echo BASE_URL; ?>index/logout">
                                <span style="float:right;">Αποσύνδεση</span>
                                <span class="logout_icon"></span>
                            </a>
                        </p>
                        <ul id="user_submenu" onmouseleave="hideusersubmenu()">
                            <li><a href="<?php echo BASE_URL; ?>userarea/album">Προσωπικό άλμπουμ</a></li>
                            <li><a href="<?php echo BASE_URL; ?>userarea/uploadphoto">Ανέβασμα φωτογραφίας</a></li>
                            <li><a href="<?php echo BASE_URL; ?>userarea/uploadphotos">Μαζικό ανέβασμα φωτογραφίών</a></li>
                            <li><a href="<?php echo BASE_URL; ?>userarea/editaccount">Τροποποίηση στοιχείων λογαριασμού</a></li>
                        </ul>
                        <script type="text/javascript">
                        function showusersubmenu() {
                            document.getElementById("user_submenu").style.display = "block";
                        }
                        function hideusersubmenu() {
                            document.getElementById("user_submenu").style.display = "none";
                        }
                        </script>
                    <?php
                    }
                    else
                    {
                    ?>
                        <form action="<?php echo BASE_URL; ?>index/login" method="post" autocomplete="off">
                            <input type="hidden" name="current_url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
                            <label for="username_login">Χρήστης</label>
                            <input type="text" id="username_login" name="username" />
                            <label for="password_login">Κωδικός</label>
                            <input type="password" id="password_login" name="password" />
                            <input class="button" type="submit" value="Είσοδος" />
                            <span>&nbsp;ή&nbsp;</span>
                            <a href="<?php echo BASE_URL; ?>register">Εγγραφή</a>
                        </form>
                    <?php
                    }
                    ?>
                    </div>
                    <?php
                    if (Session::get('logged_in'))
                    {
                        $quota_usage_in_MB = round(Session::get('quota_usage')/1048576, 2);
                        $quota_usage_percentage = $quota_usage_in_MB / QUOTA_SIZE_MB;
                        $quota_usage_in_MB_display_string = "Έχετε ".(QUOTA_SIZE_MB-$quota_usage_in_MB)." MB διαθέσιμο χώρο";
                    ?>
                    <div id="album_size_info">
                        <p>
                            <strong>Χρησιμοποιούμενος χώρος</strong>
                            <canvas id="my_canvas" width="220" height="22" title="<?php echo $quota_usage_in_MB_display_string ?>">
                                <?php echo $quota_usage_percentage; ?>
                            </canvas>
                            <script type="text/javascript">
                                var canvas = document.getElementById("my_canvas");
                                var width = canvas.width;
                                var height = canvas.height;
                                var usage = canvas.textContent * width;
                                var context = canvas.getContext("2d");
                                var gradient = context.createLinearGradient(0, 0, width, height);
                                gradient.addColorStop(0, "#B4E4F1");
                                gradient.addColorStop(0.5, "#05A4D1");
                                gradient.addColorStop(1, "#02313F");
                                context.fillStyle = gradient;
                                context.fillRect(0, 0, usage, height);
                            </script>
                        </p>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>