<!-- Main Content -->
<div id="main">
    <!-- Actual Content -->
    <div id="content">
        <h1>Εγγραφή</h1>
        <br />
        <p>Συμπληρώστε παρακάτω τα στοιχεία σας για να εγγραφείτε στο σύστημα</p>
        <br />
        <div id="register">
            <form action="<?php echo BASE_URL; ?>register/run" onsubmit="return validateUserInput(this)" method="post">
                <br />
                <p>
                    <span><label for="username">* Όνομα χρήστη (username)</label></span>
                    <input id="username" name="username" type="text" />
                </p>
                <p>
                    <span><label for="password">* Κωδικός πρόσβασης (password)</label></span>
                    <input id="password" name="password" type="password" />
                </p>
                <p>
                    <span><label for="password2">* Κωδικός πρόσβασης (επιβεβαίωση)</label></span>
                    <input id="password2" name="password2" type="password" />
                </p>
                <p>
                    <span><label for="email">* e-mail</label></span>
                    <input id="email" name="email" type="text" />
                </p>
                <br style="clear:left;"/>
                <p class="buttons">
                    <input class="button" type="reset" value="Καθαρισμός" />
                    <input class="button" type="submit" value="Αποστολή" />
                </p>
            </form>
        </div>
        <script type="text/javascript" src="<?php echo BASE_URL;?>controllers/js/register.js"></script>
    </div>
    <div id="content_bottom" style="width:70%;"></div>
</div>
