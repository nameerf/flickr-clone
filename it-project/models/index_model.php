<?php

class IndexModel extends PhotoModel
{

    function __construct()
    {
        parent::__construct();
    }
    
    public function fetchPopPhotos($start_index, $num_of_records)
    {
        $sql = "SELECT pid, path, title, width, height
                FROM photos WHERE privateness = :privateness ORDER BY views DESC LIMIT :start, :records";
        
        $values_array = array(':privateness' => array("public", PDO::PARAM_STR),
                              ':start' => array((int) $start_index, PDO::PARAM_INT),
                              ':records' => array((int) $num_of_records, PDO::PARAM_INT)); 
        
        return $this->database->select($sql, $values_array, 2, "all");
    }
    
    function login()
    {
        $sql = "SELECT users.uid, _usage FROM users INNER JOIN quota ON users.uid = quota.uid
                WHERE username = :username AND password = sha1(:password)";
        
        $values_array = array(':username' => $_POST['username'], ':password' => $_POST['password']);
        
        $returned_data = $this->database->select($sql, $values_array, 1, "one");

        if (!empty($returned_data)) {
            // set user login variables
            Session::init();
            Session::set('logged_in', true);
            Session::set('user', $_POST['username']);
            Session::set('uid', $returned_data['uid']);
            Session::set('quota_usage', $returned_data['_usage']);
        }
    }

}
