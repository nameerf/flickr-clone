<?php

class AjaxModel extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    function fetchPhotosNearUser($south_west_lat, $south_west_lng, $north_east_lat, $north_east_lng)
    {        
        $sql = "SELECT pid, path, title, address, latitude, longitude FROM photos
                WHERE privateness = :privateness
                  AND latitude >= :south_west_lat AND latitude < :north_east_lat
                  AND longitude >= :south_west_lng AND longitude < :north_east_lng";
        
        $values_array = array(':privateness' => "public",
                              ':south_west_lat' => $south_west_lat, ':north_east_lat' => $north_east_lat,
                              ':south_west_lng' => $south_west_lng, ':north_east_lng' => $north_east_lng);
        
        return $this->database->select($sql, $values_array);
    }
    
    function checkUsername($username)
    {
        $sql = "SELECT count(*) as matches FROM users WHERE username = :username";
        
        return $this->database->select($sql, array(':username' => $username), 1, "one");
    }
    
    function login($username, $password)
    {
        $sql = "SELECT users.uid, _usage FROM users INNER JOIN quota ON users.uid = quota.uid
                WHERE username = :username AND password = sha1(:password)";
        
        $values_array = array(':username' => $username, ':password' => $password);
        
        $returned_data = $this->database->select($sql, $values_array, 1, "one");

        if (!empty($returned_data)) {
            // set user session variables
            Session::init();
            Session::set('logged_in', true);
            Session::set('user', $username);
            Session::set('uid', $returned_data['uid']);
            Session::set('quota_usage', $returned_data['_usage']);
        }
    }
    
    function fetchDistinctTags($query)
    {
        $sql = "SELECT DISTINCT LOWER(tag) as tag FROM tags";
        
        $returned_data = $this->database->select($sql);
        
        $suggestions = "";
        $query_low = strtolower($query);
        for ($i=0; $i<count($returned_data); $i++)
        {
            if ($query_low == substr($returned_data[$i]['tag'], 0, strlen($query_low)))
            {
                if ($suggestions == "") {
                    $suggestions = $returned_data[$i]['tag'];
                }
                else {
                    $suggestions .= ",".$returned_data[$i]['tag'];
                }
            }
        }
        
        //output the response
        echo $suggestions;
    }
    
}
