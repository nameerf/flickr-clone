<?php

class UserAreaModel extends PhotoModel
{

    function __construct()
    {
        parent::__construct();
    }

    function fetchUserPhotos()
    {
        Session::init();
        $uid = Session::get('uid');
    
        $sql = "SELECT pid, path FROM photos WHERE uid = :uid";
        
        return $this->database->select($sql, array(':uid' => $uid));
    }
    
    function updateQuotaUsage($uid, $size)
    {
        Session::init();
        $current_usage = Session::get('quota_usage');
        Session::set('quota_usage', $current_usage+$size);
        
        $sql = "UPDATE quota SET _usage = :usage WHERE uid = :uid";
        
        $this->database->update($sql, array(':uid' => $uid, ':usage' => ($current_usage+$size)));
    }
    
    function savePhoto($path, $title, $description, $size, $width, $height, $tags, $privateness, $address, $latitude, $longitude)
    {
        Session::init();
        $uid = Session::get('uid');
        
        $sql = "INSERT INTO photos (uid, path, title, description, size, width, height, privateness, address, latitude, longitude)
                VALUES (:uid, :path, :title, :description, :size, :width, :height, :privateness, :address, :latitude, :longitude)";
        $values_array = array(':uid' => $uid, ':path' => $path, ':title' => $title, ':description' => $description, ':size' => $size, ':width' => $width,
                              ':height' => $height, ':privateness' => $privateness, ':address' => $address, ':latitude' => $latitude, ':longitude' => $longitude);
        $this->database->insert($sql, $values_array);
        
        $sql = "INSERT INTO tags (tag, photoid)
                VALUES ";
        for ($i=1; $i<=count($tags); $i++)
            $sql .= "(:tag".$i.", :photoid".$i."), ";
		//remove trailing ", "
        $sql = substr($sql, 0, -2);
        
        $returned_data = $this->database->select("SELECT pid FROM photos WHERE path = :path", array(':path' => $path), 1, "one");
		$photo_id = $returned_data['pid'];
        
        $values_array = array();
        for ($i=1; $i<=count($tags); $i++) {
            $values_array[':tag'.$i] = array($tags[$i-1], PDO::PARAM_STR);
            $values_array[':photoid'.$i] = array((int) $photo_id, PDO::PARAM_INT);
        }
        
        $this->database->insert($sql, $values_array, 2);
    }
    
    function updatePhoto($pid, $title, $description, $tags, $privateness, $address, $latitude, $longitude)
    {
        $sql = "UPDATE photos
                SET title = :title, description = :description, privateness = :privateness,
                    address = :address, latitude = :latitude, longitude = :longitude
                WHERE pid = :pid";
        $values_array = array(':pid' => $pid, ':title' => $title, ':description' => $description, ':privateness' => $privateness,
                              ':address' => $address, ':latitude' => $latitude, ':longitude' => $longitude);
        $this->database->insert($sql, $values_array);
        
		$numOfTagsDeleted = $this->database->delete("DELETE FROM tags WHERE photoid = $pid");
		
        $sql = "INSERT INTO tags (tag, photoid)
                VALUES ";
        for ($i=1; $i<=count($tags); $i++)
            $sql .= "(:tag".$i.", :photoid".$i."), ";
		//remove trailing ", "
		$sql = substr($sql, 0, -2);
                
        $values_array = array();
        for ($i=1; $i<=count($tags); $i++) {
            $values_array[':tag'.$i] = array($tags[$i-1], PDO::PARAM_STR);
            $values_array[':photoid'.$i] = array((int) $pid, PDO::PARAM_INT);
        }
        
        $this->database->insert($sql, $values_array, 2);
    }
    
    function deletePhoto($pid)
    {   
        $sql = "SELECT path, size FROM photos WHERE pid = :pid";
        $returned_data = $this->database->select($sql, array(':pid' => $pid), 1, "one");
        $normal_photo_path = $returned_data['path'];
        $size = $returned_data['size'];
        
        $position = strrpos($normal_photo_path, "/");
        $small_photo_path = substr_replace($normal_photo_path, "/small/sm_", $position+1, 0);
        $thumb_photo_path = substr_replace($normal_photo_path, "/thumb/th_", $position+1, 0);
        $square_photo_path = substr_replace($normal_photo_path, "/square/sq_", $position+1, 0);
        
        $success1 = unlink($normal_photo_path);
        $success2 = unlink($small_photo_path);
        $success3 = unlink($thumb_photo_path);
        $success4 = unlink($square_photo_path);
        
        $success5 = $this->database->delete("DELETE FROM photos WHERE pid = $pid");
        
        $numOfTagsDeleted = $this->database->delete("DELETE FROM tags WHERE photoid = $pid");
        
        $this->database->delete("DELETE FROM comments WHERE photoid = $pid");
        
        Session::init();
        $user_id = Session::get('uid');
        $current_usage = Session::get('quota_usage');
        Session::set('quota_usage', $current_usage-$size);
        
        $sql = "UPDATE quota SET _usage = :usage WHERE uid = :uid";
        $values_array = array(':uid' => $user_id, ':usage' => ($current_usage-$size));
        $this->database->update($sql, $values_array);
        
        if ($success1 && $success2 && $success3 && $success4 && $success5)
            return true;
        else
            return false;
    }
    
    function updateUserAccount()
    {
        if ($_POST["email"] == "") {
            $this->updatePasswordOnly();
        }
        else if ($_POST["password"] == "") {
            $this->updateEmailOnly();
        }
        else {
            $this->updateAll();
        }
    }

    private function updatePasswordOnly()
    {
        $password = addslashes($_POST["password"]);
        $username = Session::get('user');
        
        $sql = "UPDATE users SET password = sha1(:password) WHERE username = :username";
        
        $values_array = array(':username' => $username, ':password' => $password);
        
        $this->database->update($sql, $values_array);
    }
    
    private function updateEmailOnly()
    {
        $email = addslashes($_POST["email"]);
        $username = Session::get('user');
        
        $sql = "UPDATE users SET email = :email WHERE username = :username";
        
        $values_array = array(':username' => $username, ':email' => $email);
        
        $this->database->update($sql, $values_array);
    }
    
    private function updateAll()
    {
        $password = addslashes($_POST["password"]);
        $email = addslashes($_POST["email"]);
        $username = Session::get('user');
        
        $sql = "UPDATE users SET password = sha1(:password), email = :email WHERE username = :username";
        
        $values_array = array(':username' => $username, ':password' => $password, ':email' => $email);
        
        $this->database->update($sql, $values_array);
    }

}
