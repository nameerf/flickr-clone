<?php

class PhotosModel extends PhotoModel
{

    function __construct()
    {
        parent::__construct();
    }
    
    function updatePhotoViews($pid, $current_views)
    {
        $sql = "UPDATE photos SET views = :views WHERE pid = :pid";
        
        $values_array = array(':pid' => $pid, ':views' => ($current_views+1));
        
        $this->database->update($sql, $values_array);
    }
    
    function saveUserComment($pid, $comment)
    {
        Session::init();
        $user = Session::get('user');
        
        $sql = "INSERT INTO comments (user, photoid, comment) VALUES (:user, :photoid, :comment)";
        
        $values_array = array(':user' => $user, ':photoid' => $pid, ':comment' => $comment);
        
        $this->database->insert($sql, $values_array);
    }
    
    function fetchUsersCommentsOnPhoto($pid)
    {        
        $sql = "SELECT user, comment FROM comments WHERE photoid = :pid";
                 
        $values_array = array(':pid' => $pid);
        
        return $this->database->select($sql, $values_array);
    }

}
