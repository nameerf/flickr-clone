<?php

class RegisterModel extends Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    function registerUser()
    {
        $username = addslashes($_POST["username"]);
        $password = addslashes($_POST["password"]);
        $email = addslashes($_POST["email"]);
    
        $sql = "INSERT INTO users (username, password, email) VALUES (:username, sha1(:password), :email)";
        $values_array = array(':username' => $username, ':password' => $password, 'email' => $email);
        $this->database->insert($sql, $values_array);
        
        $sql = "SELECT uid FROM users WHERE username = :username";
        $values_array = array(':username' => $username);
        $returned_data = $this->database->select($sql, $values_array, 1, "one");
        $user_id = $returned_data['uid'];
        
        $sql = "INSERT INTO quota (uid) VALUES (:uid)";
        $this->database->insert($sql, array(':uid' => $user_id));
        
        // set user session variables
        Session::init();
        Session::set('logged_in', true);
        Session::set('user', $username);
        Session::set('uid', $user_id);
        Session::set('quota_usage', 0);
    }

}
