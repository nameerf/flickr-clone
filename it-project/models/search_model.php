<?php

class SearchModel extends PhotoModel
{

    function __construct()
    {
        parent::__construct();
    }

    function fetchPhotosWithTag($tag, $start_index, $num_of_records)
    {
        $sql = "SELECT count(*) as matches FROM photos INNER JOIN tags ON pid = photoid
                WHERE privateness = :privateness AND tag = :tag";

        $values_array = array(':privateness' => "public", ':tag' => $tag);
    
        $returned_data = $this->database->select($sql, $values_array, 1, "one");
        
        
        $sql = "SELECT * FROM photos INNER JOIN tags ON pid = photoid
                WHERE privateness = :privateness AND tag = :tag ORDER BY views DESC LIMIT :start, :records";
        
        $values_array = array(':privateness' => array("public", PDO::PARAM_STR),
                              ':tag' => array($tag, PDO::PARAM_STR),
                              ':start' => array((int) $start_index, PDO::PARAM_INT),
                              ':records' => array((int) $num_of_records, PDO::PARAM_INT)); 
        
        return array($returned_data['matches'], $this->database->select($sql, $values_array, 2));
    }
    
    function fetchPhotosWithQuery($search_with, $query, $start_index, $num_of_records)
    {   
        $num_of_words = 1;
        
        if ($search_with == "text") {
            $words = explode(" ", $query);
            $num_of_words = count($words);
        }
        
        if ($num_of_words > 1) {
            return $this->multipleWordQuery($search_with, $num_of_words, $words, $start_index, $num_of_records);
        }
        else if ($num_of_words == 1) {
            if ($search_with == "tags") {
                return $this->fetchPhotosWithTag($query, $start_index, $num_of_records);
            }
            else if ($search_with == "text") {
                return $this->singleWordQuery($query, $start_index, $num_of_records);
            }
        }
    }

    function singleWordQuery($query, $start_index, $num_of_records)
    {
        $sql = "SELECT count(*) as matches FROM photos INNER JOIN (SELECT DISTINCT pid FROM photos INNER JOIN tags ON pid = photoid
                WHERE privateness = :privateness AND (title LIKE :title OR description LIKE :description OR tag = :tag)) AS t2
                ON photos.pid = t2.pid";

        $values_array = array(':privateness' => array("public", PDO::PARAM_STR),
                              ':title' => array("%".$query."%", PDO::PARAM_STR),
                              ':description' => array("%".$query."%", PDO::PARAM_STR),
                              ':tag' => array($query, PDO::PARAM_STR));
    
        $returned_data = $this->database->select($sql, $values_array, 2, "one");
                
        $sql = "SELECT * FROM photos INNER JOIN (SELECT DISTINCT pid FROM photos INNER JOIN tags ON pid = photoid
                WHERE privateness = :privateness AND (title LIKE :title OR description LIKE :description OR tag = :tag)) AS t2
                ON photos.pid = t2.pid
                ORDER BY views DESC LIMIT :start, :records";
        
        $values_array = array(':privateness' => array("public", PDO::PARAM_STR),
                              ':title' => array("%".$query."%", PDO::PARAM_STR),
                              ':description' => array("%".$query."%", PDO::PARAM_STR),
                              ':tag' => array($query, PDO::PARAM_STR),
                              ':start' => array((int) $start_index, PDO::PARAM_INT),
                              ':records' => array((int) $num_of_records, PDO::PARAM_INT)); 
        
        return array($returned_data['matches'], $this->database->select($sql, $values_array, 2));
    }
    
    function multipleWordQuery($search_with, $num_of_words, $words, $start_index, $num_of_records)
    {
        /*
        if ($search_with == "tags")
        {   
            $sql_count = "SELECT count(*) as matches FROM photos WHERE pid in (SELECT t1.photoid FROM tags AS t1";
            $sql = "SELECT * FROM photos WHERE pid in (SELECT t1.photoid FROM tags AS t1";
            for ($i=0; $i<$num_of_words-1; $i++) {
                $sql_count .= " INNER JOIN tags AS t".($i+2);
                $sql .= " INNER JOIN tags AS t".($i+2);
            }
            $sql_count .= " ON t1.photoid";
            $sql .= " ON t1.photoid";
            for ($i=0; $i<$num_of_words-1; $i++) {
                $sql_count .= " = t".($i+2).".photoid";
                $sql .= " = t".($i+2).".photoid";
            }
            $sql_count .= " WHERE t1.tag = :tag1 ";
            $sql .= " WHERE t1.tag = :tag1 ";
            for ($i=0; $i<$num_of_words-1; $i++) {
                $sql_count .= " AND t".($i+2).".tag = :tag".($i+2);
                $sql .= " AND t".($i+2).".tag = :tag".($i+2);
            }
            $sql_count .= ") AND privateness = :privateness";
            $sql .= ") AND privateness = :privateness ORDER BY views DESC LIMIT :start, :records";
            
            $values_array = array();
            for ($i=0; $i<$num_of_words; $i++) {
                $values_array[':tag'.($i+1)] = array($words[$i], PDO::PARAM_STR);
            }
            $values_array[':privateness'] = array("public", PDO::PARAM_STR);
            
            $returned_data = $this->database->select($sql_count, $values_array, 2, "one");
            
            $values_array = array();
            for ($i=0; $i<$num_of_words; $i++) {
                $values_array[':tag'.($i+1)] = array($words[$i], PDO::PARAM_STR);
            }
            $values_array[':privateness'] = array("public", PDO::PARAM_STR);
            $values_array[':start'] = array((int) $start_index, PDO::PARAM_INT);
            $values_array[':records'] = array((int) $num_of_records, PDO::PARAM_INT);
            
            return array($returned_data['matches'], $this->database->select($sql, $values_array, 2));
        }
        */
        if ($search_with == "text")
        {
            $sql_count = "SELECT count(*) as matches FROM photos INNER JOIN (SELECT DISTINCT pid FROM photos INNER JOIN tags ON pid = photoid
                          WHERE privateness = :privateness AND (title LIKE :title1 OR description LIKE :description1 OR tag = :tag1) ";
            $sql = "SELECT * FROM photos INNER JOIN (SELECT DISTINCT pid FROM photos INNER JOIN tags ON pid = photoid
                    WHERE privateness = :privateness AND (title LIKE :title1 OR description LIKE :description1 OR tag = :tag1) ";
            for ($i=0; $i<$num_of_words-1; $i++) {
                $sql_count .= " AND (title LIKE :title".($i+2)." OR description LIKE :description".($i+2)." OR tag = :tag".($i+2).")";
                $sql .= " AND (title LIKE :title".($i+2)." OR description LIKE :description".($i+2)." OR tag = :tag".($i+2).")";
            }
            $sql_count .= ") AS t2 ON photos.pid = t2.pid";
            $sql .= ") AS t2 ON photos.pid = t2.pid ORDER BY views DESC LIMIT :start, :records";
            
            $values_array = array();
            for ($i=0; $i<$num_of_words; $i++) {
                $values_array[':title'.($i+1)] = array("%".$words[$i]."%", PDO::PARAM_STR);
                $values_array[':description'.($i+1)] = array("%".$words[$i]."%", PDO::PARAM_STR);
                $values_array[':tag'.($i+1)] = array($words[$i], PDO::PARAM_STR);
            }
            $values_array[':privateness'] = array("public", PDO::PARAM_STR);
            
            $returned_data = $this->database->select($sql_count, $values_array, 2, "one");
            
            $values_array = array();
            for ($i=0; $i<$num_of_words; $i++) {
                $values_array[':title'.($i+1)] = array("%".$words[$i]."%", PDO::PARAM_STR);
                $values_array[':description'.($i+1)] = array("%".$words[$i]."%", PDO::PARAM_STR);
                $values_array[':tag'.($i+1)] = array($words[$i], PDO::PARAM_STR);
            }
            $values_array[':privateness'] = array("public", PDO::PARAM_STR);
            $values_array[':start'] = array((int) $start_index, PDO::PARAM_INT);
            $values_array[':records'] = array((int) $num_of_records, PDO::PARAM_INT);
            
            return array($returned_data['matches'], $this->database->select($sql, $values_array, 2));
        }
        else {
            //TODO fix me
            echo "ERROR...";
        }
    }

}
