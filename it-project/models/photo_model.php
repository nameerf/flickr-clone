<?php

class PhotoModel extends Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    function fetchFromPhoto($pid, $columns_string)
    {
        $sql = "SELECT ".$columns_string." FROM photos WHERE pid = :pid";
        
        return $this->database->select($sql, array(':pid' => $pid), 1, "one");
    }

    function fetchPhotoTags($pid)
    {
        $sql = "SELECT tag FROM tags WHERE photoid = :photoid";
        
        return $this->database->select($sql, array(':photoid' => $pid));
    }
    
    function fetchPhotoOwner($pid)
    {
        $sql = "SELECT username FROM users INNER JOIN photos ON users.uid = photos.uid WHERE pid = :pid";
        
        $returned_data = $this->database->select($sql, array(':pid' => $pid), 1, "one");
        
        return $returned_data['username'];
    }
    
    function fetchTags()
    {
        $sql = "SELECT LOWER(tag) AS tag FROM tags INNER JOIN photos ON photoid = pid WHERE privateness = :privateness";
        
        return $this->database->select($sql, array(':privateness' => "public"));
    }

}
