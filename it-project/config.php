<?php

// path constants 
define("BASE_URL", "http://localhost/it-project/");
define("ROOT_DIR_NAME", "/it-project/");
define("PHOTOS_DIR_NAME", "photos/");
define("APP_DIR_NAME", "application/");

// various constants
define("IMG_MAX_WIDTH", 1280);
define("IMG_MAX_HEIGHT", 1024);
define("QUOTA_SIZE_MB", 50);
define("QUOTA_SIZE_BYTES", 52428800);
define("TOP_POP_PHOTOS", 10);
define("MAX_PHOTOS_PER_PAGE", 9);
define("MAX_UPLOADED_FILE_SIZE", 52428800); // maximum size of uploaded files in general = 50MB (need it for zip archive)
define("MAX_UPLOADED_IMAGE_SIZE", 3145728); // maximum size of uploaded images = 3MB
define("MAX_TAGS_OF_TAGCLOUD", 100);
define("MAX_TAG_SUGGESTIONS", 5);

// database constants 
define("DB_TYPE", "mysql");
define("DB_HOST", "localhost");
define("DB_NAME", "itproject");
define("DB_USER", "root");
define("DB_PASS", "r5tfde4");
