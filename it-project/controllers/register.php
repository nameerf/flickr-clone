<?php

class Register extends Controller
{

    function __construct()
    {
        parent::__construct();
        Session::init();
        $logged_in = Session::get('logged_in');
        if ($logged_in == true) {
            header('location: '.BASE_URL.'error/registernotaccessible');
        }
    }

    function index()
    {
        $this->view->style = "registereditaccount";
        $this->view->render('register/index');
    }

    function run()
    {
        $this->model->registerUser();
        
        Session::init();
        $user = Session::get('user');
        $photos_path = $_SERVER['DOCUMENT_ROOT'].ROOT_DIR_NAME.PHOTOS_DIR_NAME;
        if (!is_dir($photos_path."$user")) {
            mkdir($photos_path."$user", 0777);
            mkdir($photos_path."$user/small", 0777);
            mkdir($photos_path."$user/thumb", 0777);
            mkdir($photos_path."$user/square", 0777);
        }
        
        header('location: '.BASE_URL.'userarea');
    }

}
