<?php

class UserArea extends Controller
{

    function __construct()
    {
        parent::__construct();
        Session::init();
        $logged_in = Session::get('logged_in');
        if ($logged_in == false) {
            Session::destroy();
            header('location: '.BASE_URL.'error/authorizationrequired/user');
        }
    }

    function index()
    {
        $this->view->style = "userarea";
        $this->view->render('userarea/index');
    }

    function album()
    {
        $this->view->style = "album";
        $photos = $this->model->fetchUserPhotos();

        $this->view->photos_pids = array();
        $this->view->photos_urls = array();
        $this->view->thumbs_urls = array();
        for ($i=0; $i<count($photos); $i++) {
            $this->view->photos_pids[$i] = $photos[$i]['pid'];
            $this->view->photos_urls[$i] = $photos[$i]['path'];
            $position = strrpos($photos[$i]['path'], "/");
            $newstring = substr_replace($photos[$i]['path'], "square/sq_", $position+1, 0);
            $this->view->thumbs_urls[$i] = $newstring;
        }

        $this->view->render('userarea/album');
    }

    function uploadPhoto()
    {
        $this->view->style = "uploadedit";
        $this->view->render('userarea/uploadphoto');
    }

    function uploadAndSavePhoto()
    {
        list($img_path, $img_size, $img_width, $img_height) = $this->resizeAndSaveImageToDisk("uploaded_image");

        $title = addslashes($_POST["title"]);
        $description = addslashes($_POST["description"]);
        $privateness = addslashes($_POST["privateness"]);

        $tags = array();
        $i = 0;
        while (isset($_POST["tag".($i+1)]))
        {
            $tags[$i] = addslashes($_POST["tag".($i+1)]);
            $i++;
        }

        if (isset($_POST["city"]) && $_POST["city"] != "" && isset($_POST["street"]) && $_POST["street"] != "" && isset($_POST["street_number"])) {

            $photo_ref_address = $_POST["street_number"]."+".$_POST["street"]."+".$_POST["city"];

            $coordinates = addressToCoordinates($photo_ref_address);

            $latitude = $coordinates["latitude"];
            $longitude = $coordinates["longitude"];
        }
        else {
        	$photo_ref_address = NULL;
            $latitude = NULL;
            $longitude = NULL;
        }

        $this->model->savePhoto($img_path, $title, $description, $img_size, $img_width, $img_height,
                                $tags, $privateness, $photo_ref_address, $latitude, $longitude);

        Session::init();
        $uid = Session::get('uid');
        $this->model->updateQuotaUsage($uid, $img_size);

        header('location: '.BASE_URL.'userarea');
    }

    function uploadPhotos()
    {
        $this->view->style = "uploadedit";
        $this->view->render('userarea/uploadphotos');
    }

    function uploadAndSavePhotos()
    {
        $uploaded_file_name = $_FILES["uploaded_zipped_file"]["name"];
        $uploaded_file_type = $_FILES["uploaded_zipped_file"]["type"];
        $uploaded_file_size = $_FILES["uploaded_zipped_file"]["size"];
        $uploaded_file_tmp_name = $_FILES["uploaded_zipped_file"]["tmp_name"];
        $error = $_FILES["uploaded_zipped_file"]["error"];

        if (($uploaded_file_type == "application/zip") && ($uploaded_file_size < MAX_UPLOADED_FILE_SIZE))
        {
            if ($error > 0) {
                echo "Error: " . $error . "<br />";
                die;
            }
            else
            {
                Session::init();
                $user = Session::get('user');
                $user_id = Session::get('uid');
                $current_usage = Session::get('quota_usage');

                $photos_path = $_SERVER['DOCUMENT_ROOT'].ROOT_DIR_NAME.constant('PHOTOS_DIR_NAME');
                //$tmp_dir = "/tmp/$user/";     //linux only
                $tmp_dir = $photos_path."$user/tmp/";
                if (!is_dir($tmp_dir)) {
                    mkdir($tmp_dir, 0777);
                }

                echo shell_exec("unzip $uploaded_file_tmp_name -d $tmp_dir") . "<br /><br />";

                $xml = simplexml_load_file($tmp_dir."info.xml");

                $total_images_size = 0;
                $quota_usage_full = false;

                foreach($xml->children() as $child)
                {
                    switch($child->getName())
                    {
                        case "filename":
                            $img_filename = $child;
                            break;
                        case "title":
                            $title = $child;
                            break;
                        case "description":
                            $description = $child;
                            break;
                        case "tags":
                            $tags = array();
                            $i = 0;
                            foreach($child->children() as $tag) {
                                if ($tag != "" && $i < 10) {
                                    $tags[$i++] = $tag;
                                }
                            }
                            break;
                        case "privateness":
                            if ($child == "ιδιωτική") {
                                $privateness = "private";
                            }
                            else {
                                $privateness = "public";
                            }
                            break;
                        case "location":
                            $address_parts = array();
                            $i = 0;
                            foreach($child->children() as $address_part) {
                                $address_parts[$i++] = $address_part;
                            }

                            $photo_ref_address = $address_parts[1]."+".$address_parts[0]."+".$address_parts[2];

                            $coordinates = addressToCoordinates($photo_ref_address);

                            $latitude = $coordinates["latitude"];
                            $longitude = $coordinates["longitude"];

                            if (($img_filename != "") && ($title != "") && ($description != "") && (count($tags) > 0) && ($privateness != ""))
                            {
                                $img_tmp_name = $tmp_dir.$img_filename;
                                $file_info = new finfo(FILEINFO_MIME_TYPE);  // object oriented approach
                                $img_mime_type = $file_info->buffer(file_get_contents($img_tmp_name));  // e.g. gives "image/jpeg
                                $img_size = filesize($img_tmp_name);

                                list($img_path, $saved_img_size, $img_width, $img_height) = $this->resizeAndSaveImageToDisk(NULL, $img_filename, $img_mime_type, $img_size, $img_tmp_name);

                                if ($current_usage+$total_images_size+$saved_img_size > QUOTA_SIZE_BYTES) {
                                    $quota_usage_full = true;
                                    unlink($img_path);
                                    break;
                                }
                                else {
                                    $total_images_size += $saved_img_size;
                                }

                                $this->model->savePhoto($img_path, $title, $description, $saved_img_size, $img_width, $img_height,
                                                        $tags, $privateness, $photo_ref_address, $latitude, $longitude);
                            }
                            break;
                        default:
                            echo "ERROR: xml tag not valid <br />";
                            // do proper error handling here
                    }

                    if ($quota_usage_full) {
                        break;
                    }
                }

                // clean up temp dir
                rrmdir($tmp_dir);

                $this->model->updateQuotaUsage($user_id, $total_images_size);
            }
        }
        else {
            echo "Invalid file";
            die;
            //TODO error checking and redirect
        }

        header('location: '.BASE_URL.'userarea');
    }

    function resizeAndSaveImageToDisk($uploaded_file_input_name, $img_filename = NULL, $img_mime_type = NULL, $img_size = NULL, $img_tmp_name = NULL)
    {
        if (!is_null($uploaded_file_input_name)) {
            $img_filename = $_FILES[$uploaded_file_input_name]["name"];
            $img_mime_type = $_FILES[$uploaded_file_input_name]["type"];
            $img_size = $_FILES[$uploaded_file_input_name]["size"];
            $img_tmp_name = $_FILES[$uploaded_file_input_name]["tmp_name"];
            $error = $_FILES[$uploaded_file_input_name]["error"];
        }

        if ((($img_mime_type == "image/png") || ($img_mime_type == "image/jpeg") || ($img_mime_type == "image/pjpeg")) && ($img_size < MAX_UPLOADED_IMAGE_SIZE))
        {
            if (isset($error) && $error > 0) {
                echo "Error: " . $error . "<br />";
            }
            else
            {
                Session::init();
                $user = Session::get('user');
                $user_id = Session::get('uid');

                if (file_exists(constant('PHOTOS_DIR_NAME')."$user/".$img_filename)) {
                    echo 0;
                    //TODO do proper error handling here
                }

                $img = new Image($img_tmp_name);
                $small = new Image($img_tmp_name);
                $thumb = new Image($img_tmp_name);
                $square = new Image($img_tmp_name);

                if ($img->needResize(IMG_MAX_WIDTH, IMG_MAX_HEIGHT)) {
                    list($img_new_width, $img_new_height) = $img->calculateNewSize(IMG_MAX_WIDTH, IMG_MAX_HEIGHT);
                    $img->resize($img_new_width, $img_new_height, $img->getWidth(), $img->getHeight());
                }
                /* create a small (240px biggest dimension), a thumb (100px biggest dimension)
                 * and a square (75px each dimension) version of the image */
                $img_ratio = $img->getRatio();
                if($img_ratio > 1) {
                    $small->resize(240, 240/$img_ratio, $img->getWidth(), $img->getHeight());
                    $thumb->resize(100, 100/$img_ratio, $img->getWidth(), $img->getHeight());
                    $square->cropAndResize(($img->getWidth()-$img->getHeight())/2, 0, 75, 75, $img->getHeight(), $img->getHeight());
                }
                else {
                    $small->resize(240*$img_ratio, 240, $img->getWidth(), $img->getHeight());
                    $thumb->resize(100*$img_ratio, 100, $img->getWidth(), $img->getHeight());
                    $square->cropAndResize(($img->getHeight()-$img->getWidth())/2, 0, 75, 75, $img->getWidth(), $img->getWidth());
                }

                $img_path = PHOTOS_DIR_NAME."$user/".$img_filename;
                $small_path = PHOTOS_DIR_NAME."$user/small/sm_".$img_filename;
                $thumb_path = PHOTOS_DIR_NAME."$user/thumb/th_".$img_filename;
                $square_path = PHOTOS_DIR_NAME."$user/square/sq_".$img_filename;

                $img->save($img_path, $img->getType(), 100);
                $small->save($small_path, $img->getType(), 100);
                $thumb->save($thumb_path, $img->getType(), 100);
                $square->save($square_path, $img->getType(), 100);

                // image size in bytes
                $saved_img_size = filesize($img_path);

                return array($img_path, $saved_img_size, $img_new_width, $img_new_height);
            }
        }
        else {
            echo "Invalid file";
            return -1;
            //TODO error checking and redirect
        }
    }

    function editPhoto()
    {
        $this->view->photo_id = $_GET["pid"];

        $photo = $this->model->fetchFromPhoto($this->view->photo_id, "*");
        $this->view->photo_url = $photo['path'];
        $position = strrpos($photo['path'], "/");
        $newstring = substr_replace($photo['path'], "/small/sm_", $position+1, 0);
        $this->view->small_photo_url = $newstring;
        $this->view->photo_title = stripslashes($photo['title']);
        $this->view->photo_desc = stripslashes($photo['description']);
        $photo_address = $photo['address'];
        $this->view->address_parts = explode("+", $photo_address);
        $this->view->photo_priv = $photo['privateness'];
        $this->view->photo_tags = $this->model->fetchPhotoTags($this->view->photo_id);

        $this->view->style = "uploadedit";
        $this->view->render('userarea/editphoto');
    }

    function updatePhoto()
    {
        $pid = $_POST["photo_id"];
        $title = addslashes($_POST["title"]);
        $description = addslashes($_POST["description"]);
        $privateness = addslashes($_POST["privateness"]);

        $tags = array();
        $i = 0;
        while (isset($_POST["tag".($i+1)]))
        {
            $tags[$i] = addslashes($_POST["tag".($i+1)]);
            echo "<br />" . "current tag = " . $tags[$i] . "<br />";
            $i++;
        }

        if (isset($_POST["city"]) && isset($_POST["street"]) && isset($_POST["street_number"])) {

            $photo_ref_address = $_POST["street_number"]."+".$_POST["street"]."+".$_POST["city"];

            $coordinates = addressToCoordinates($photo_ref_address);

            $latitude = $coordinates["latitude"];
            $longitude = $coordinates["longitude"];
        }
        else {
            $latitude = NULL;
            $longitude = NULL;
        }

        $this->model->updatePhoto($pid, $title, $description, $tags, $privateness, $photo_ref_address, $latitude, $longitude);

        header('location: '.BASE_URL.'userarea/album');
    }

    function deletePhoto()
    {
        if ($this->model->deletePhoto($_GET["pid"])) {
            header('location: '.BASE_URL.'userarea/album');
        }
        else
            header('location: '.BASE_URL.'error/index/deletephoto');
    }

    function editAccount()
    {
        $this->view->style = "registereditaccount";
        $this->view->render('userarea/editaccount');
    }

    function updateAccount()
    {
        $this->model->updateUserAccount();
        header('location: '.BASE_URL.'userarea');
    }

}
