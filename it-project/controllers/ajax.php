<?php

class Ajax extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function mapPhotos()
    {
        $map_coordinates = $_GET["q"];
        
        $map_coord = explode(",", $map_coordinates);
        
        $photos_data = $this->model->fetchPhotosNearUser($map_coord[0], $map_coord[1], $map_coord[2], $map_coord[3]);
        
        echo json_encode($photos_data);
    }
    
    function checkUsername()
    {
        $username = $_GET['username'];
        
        echo json_encode($this->model->checkUsername($username));
    }
    
    function autocomplete()
    {
        $query = $_GET["q"];
        
        $this->model->fetchDistinctTags($query);
    }

}
