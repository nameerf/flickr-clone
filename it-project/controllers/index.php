<?php

class Index extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $array_of_tags = $this->model->fetchTags();
        list($this->view->tags, $this->view->tag_freq, $this->view->max_freq) = $this->createTagCloud($array_of_tags);
        
        $photos = $this->model->fetchPopPhotos(0, TOP_POP_PHOTOS);
        
        $this->view->photos_pids = array();
        $this->view->photos_paths = array();
        $this->view->photos_small_paths = array();
        $this->view->photos_titles = array();
        $this->view->photos_widths = array();
        $this->view->photos_heights = array();
        $this->view->photos_owners = array();
        for ($i=0; $i<count($photos); $i++) {
            $this->view->photos_pids[$i] = $photos[$i]['pid'];
            $this->view->photos_paths[$i] = $photos[$i]['path'];
            $position = strrpos($photos[$i]['path'], "/");
            $newstring = substr_replace($photos[$i]['path'], "small/sm_", $position+1, 0);
            $this->view->photos_small_paths[$i] = $newstring;
            $this->view->photos_titles[$i] = stripslashes($photos[$i]['title']);
            $this->view->photos_widths[$i] = $photos[$i]['width'];
            $this->view->photos_heights[$i] = $photos[$i]['height'];
            $this->view->photos_owners[$i] = $this->model->fetchPhotoOwner($photos[$i]['pid']);
        }
        
        $this->view->style = "index";
        $this->view->render('index/index');
    }
    
    function login()
    {
        $this->model->login();
        
        if (Session::get('logged_in')) {
            $current_page_url = str_replace(ROOT_DIR_NAME, "", $_POST['current_url']);
            header('location: '.BASE_URL.$current_page_url);
        }
        else {
            header('location: '.BASE_URL.'error/loginfailed');
        }
    }
    
    function logout()
    {
        Session::init();
        Session::destroy();
        header('location: '.BASE_URL);
    }
    
    private function createTagCloud($tag_array)
    {
        $temp_array = array();
        
        for ($i=0; $i<count($tag_array); $i++) { 
            $temp_array[$i] = $tag_array[$i]['tag'];
        }
        
        // get unique tags only
        $unique_tags_array = array_count_values($temp_array);
        
        // sort array by values in reverse order
        arsort($unique_tags_array);
        
        // get the frequency of the most used tag
        $max_frequency = max($unique_tags_array);
        
        // get top 50 tags (by frequency)
        $most_pop_tags = array_slice($unique_tags_array, 0, MAX_TAGS_OF_TAGCLOUD);
        
        // sort array by key in natural order
        uksort($most_pop_tags, 'strnatcasecmp');

        $keys = array_keys($most_pop_tags);
        $values = array_values($most_pop_tags);
        
        return array($keys, $values, $max_frequency);
    }

}
