<?php

class Error extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index($message_type = "")
    {
        $this->view->error_type = "Σφάλμα";
        if ($message_type == "deletephoto")
            $this->view->error_message = "Υπήρξε κάποιο πρόβλημα κατά τη διαγραφή της φωτογραφίας.";
        else
            header('location: '.BASE_URL);
        $this->view->style = "main";
        $this->view->render('error/index');
    }
    
    function pagenotfound()
    {
        $this->view->error_type = "Η σελίδα δεν βρέθηκε";
        $this->view->error_message = "Φαίνεται πως ακολουθήσατε κάποιο λαναθασμένο σύνδεσμο.";
        $this->view->style = "main";
        $this->view->render('error/index');
    }    
    
    function loginfailed()
    {
        $this->view->error_type = "Λανθασμένο όνομα χρήστη ή κωδικός πρόσβασης";
        $this->view->error_message = "Παρακαλoύμε προσπαθήστε ξανά.";
        $this->view->style = "main";
        $this->view->render('error/index');
    }
    
    function registernotaccessible()
    {
        $this->view->error_type = "Λανθασμένο αίτημα";
        $this->view->error_message = "Δεν μπορείτε να δείτε αυτή τη σελίδα ενώ είστε συνδεδεμένος.";
        $this->view->style = "main";
        $this->view->render('error/index');
    }

    function accessdenied()
    {
        $this->view->error_type = "Απαγορεύτεαι η πρόσβαση";
        $this->view->error_message = "Δεν έχετε δικαίωμα να δείτε αυτή τη φωτογραφία. ".
                                     "Η φωτογραφία αυτή είναι ιδιωτική και είναι ορατή μόνο από τον κάτοχό της.";
        $this->view->style = "main";
        $this->view->render('error/index');
    }
    
    function authorizationrequired($message_type)
    {
        $this->view->error_type = "Απαιτείται εξουσιοδότηση";
        if ($message_type == "user")
            $this->view->error_message = "Πρέπει να είστε συνδεδεμένοι για να δείτε αυτή τη σελίδα.";
        else
            $this->view->error_message = "Πρέπει να είστε συνδεδεμένοι για να μπορείτε να γράψετε το σχόλιό σας.";
        $this->view->style = "main";
        $this->view->render('error/index');
    }
    
}
