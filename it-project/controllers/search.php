<?php

class Search extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->view->style = "search";
    }

    function index()
    {
        if (isset($_GET["q"])) {
            $this->view->search_query = $_GET["q"];
            
            if ($this->view->search_query == "") {
                header('location: '.BASE_URL."search");
            }
            
            if (isset($_GET["w"])) {    //it would be set as "tags"
                $this->view->search_with = $_GET["w"];
            }
            else {  // default search with full text
                $this->view->search_with = "text";
            }
            
            $this->doSearch();
        }
        else {
            $array_with_tags = $this->model->fetchTags();
                
            list($this->view->tags, $this->view->tag_freq, $this->view->max_freq) = $this->createTagCloud($array_with_tags);
            
            $this->view->render('search/index');
        }
    }

    private function createTagCloud($tag_array)
    {
        $temp_array = array();
        
        for ($i=0; $i<count($tag_array); $i++) { 
            $temp_array[$i] = $tag_array[$i]['tag'];
        }
        
        // get unique tags only
        $unique_tags_array = array_count_values($temp_array);
        
        // sort array by values in reverse order
        arsort($unique_tags_array);
        
        // get the frequency of the most used tag
        $max_frequency = max($unique_tags_array);
        
        // get top 50 tags (by frequency)
        $most_pop_tags = array_slice($unique_tags_array, 0, MAX_TAGS_OF_TAGCLOUD);
        
        // sort array by key in natural order
        uksort($most_pop_tags, 'strnatcasecmp');        

        $keys = array_keys($most_pop_tags);
        $values = array_values($most_pop_tags);
        
        return array($keys, $values, $max_frequency);
    }

    private function doSearch()
    {
        if (isset($_GET["page"])) {
            $this->view->current_page = $_GET["page"];
        }
        else {
            $this->view->current_page = 1;
        }
        
        $start_index = ($this->view->current_page*MAX_PHOTOS_PER_PAGE) - MAX_PHOTOS_PER_PAGE;
        
        switch($this->view->search_with)
        {
            case "tag":
                list($matches, $photos) = $this->model->fetchPhotosWithTag($this->view->search_query, $start_index, MAX_PHOTOS_PER_PAGE);
                break;
            case "text":
            case "tags":
                list($matches, $photos) = $this->model->fetchPhotosWithQuery($this->view->search_with, $this->view->search_query, $start_index, MAX_PHOTOS_PER_PAGE);
        }
        
        $this->view->num_of_matches = $matches;
        $this->view->photos_pids = array();
        $this->view->photos_urls = array();
        $this->view->photos_small_urls = array();
        $this->view->photos_titles = array();
        $this->view->photos_widths = array();
        $this->view->photos_heights = array();
        $this->view->photos_views = array();
        $this->view->photos_owners = array();
        for ($i=0; $i<count($photos); $i++) {
            $this->view->photos_pids[$i] = $photos[$i]['pid'];
            $this->view->photos_urls[$i] = $photos[$i]['path'];
            $position = strrpos($photos[$i]['path'], "/");
            $newstring = substr_replace($photos[$i]['path'], "/small/sm_", $position+1, 0);
            $this->view->photos_small_urls[$i] = $newstring;
            $this->view->photos_titles[$i] = stripslashes($photos[$i]['title']);
            $this->view->photos_widths[$i] = $photos[$i]['width'];
            $this->view->photos_heights[$i] = $photos[$i]['height'];
            $this->view->photos_views[$i] = $photos[$i]['views'];
            $this->view->photos_owners[$i] = $this->model->fetchPhotoOwner($photos[$i]['pid']);
        }
        
        $this->view->render('search/results');
    }
    
}
