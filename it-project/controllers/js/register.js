
function usernameAlreadyUsed()
{
	username = document.getElementById('username').value;
	
	var xmlhttp;
	
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	else // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	
	var username_used = "sdfsdfs";
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var result = JSON.parse(xmlhttp.responseText);
			
			username_used = (result.matches == 1) ? true : false;
		}
	}
		
	xmlhttp.open("GET", "http://localhost/it-project/ajax/checkusername?username="+username, false);
	xmlhttp.send();
	
	return username_used;
}

function validateUserInput(form)
{
	var username;
	try {
		username = form["username"].value;
	} catch (err) {
		username = null;
	}
	var password = form["password"].value;
	var password2 = form["password2"].value;
	var email = form["email"].value;

	var index = 0;
	var error_strings = new Array();
	
	if(username != null) {
		if(isEmpty(username)) {
			error_strings[index++] = "Το όνομα χρήστη πρέπει να συμπληρωθεί.";
		}
		else if(!isValidLength(username, 2, 21)) {
			error_strings[index++] = "Το όνομα χρήστη πρέπει να είναι μεταξύ 3 και 20 χαρακτήρες.";
		}
		else if(!isAlphanumeric(username)) {
			error_strings[index++] = "Το όνομα χρήστη πρέπει να περιέχει μόνο γράμματα (με λατινικούς χαρακτήρες) και αριθμούς.";
		}
		else if(usernameAlreadyUsed()) {
			error_strings[index++] = "Το όνομα χρήστη που εισάγατε χρησιμοποιείται ήδη. Παρακαλώ δοκιμάστε κάποιο άλλο.";
		}
		if(isEmpty(password)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης πρέπει να συμπληρωθεί.";
		}
		else if(!isValidLength(password, 4, 31)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης πρέπει να είναι μεταξύ 5 και 30 χαρακτήρες.";
		}
		else if(!passwordContainsValidCharacters(password)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης περιέχει μη έγκυρους χαρακτήρες.";
		}
		else if(password != password2 || isEmpty(password2)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης δεν επιβεβαιώθηκε.";
		}
	}
	else if(!isEmpty(password)) {
		if(!isValidLength(password, 4, 31)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης πρέπει να είναι μεταξύ 5 και 30 χαρακτήρες.";
		}
		else if(password != password2 || isEmpty(password2)) {
			error_strings[index++] = "Ο κωδικός πρόσβασης δεν επιβεβαιώθηκε.";
		}
	}

	if(isEmpty(email)) {
		error_strings[index++] = "Το e-mail πρέπει να συμπληρωθεί.";
	}
	else if(!isValidEmail(email)) {
		error_strings[index++] = "Το e-mail δεν είναι έγκυρο.";
	}

	if(error_strings.length == 0) {
		return true;
	}

	var error = document.getElementById("content_bottom");

	var error_message = "<p>Τα στοιχεία που δώσατε περιέχουν λάθη. Παρακαλώ διωρθώστε τα και ξαναπροσπαθήστε.</p><br />";
	error_message += "<ul>";
	for(var i=0; i<error_strings.length; i++) {
		if(error_strings[i] === undefined) {
			continue;
		}
		error_message += "<li>" + error_strings[i] + "</li>";
	}
	error_message += "</ul>";

	error.innerHTML = error_message;

	return false;
}

function isEmpty(elem_value)
{
	return empty = (elem_value.length == 0) ? true : false;
}

function isValidLength(elem_value, minValue, maxValue)
{
	return validLength = (elem_value.length > minValue && elem_value.length < maxValue) ? true : false;
}

function isAlphanumeric(elem_value)
{
	var alphaExp = /^[0-9a-zA-Z_]+$/;
	return alphanumeric = (elem_value.match(alphaExp)) ? true : false;
}

// this to avoid UTF-8 characters
function passwordContainsValidCharacters(elem_value)
{
	var validPassCharsExp = /^[0-9a-zA-Z\ \`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\[\]\{\}\;\"\'\,\.\<\>\?]+$/;
	return validPassChars = (elem_value.match(validPassCharsExp)) ? true : false;
}

function isValidEmail(elem_value)
{
	var validEmailExp = /^[a-zA-Z0-9\._\-]+@([a-zA-Z0-9][a-zA-Z0-9\-]*\.)+[a-zA-Z]+$/;
	return validEmail = (elem_value.match(validEmailExp)) ? true : false;
}
