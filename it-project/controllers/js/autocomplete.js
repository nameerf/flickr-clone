
function showSuggestions(input)
{
	var xmlhttp;
	
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	else // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var ul_id = document.activeElement.parentNode.children[1].id;
			
			var ul_element = document.getElementById(ul_id);
			
			if (input.length == 0) {
				ul_element.style.display = "none";
				return;
			}
			
			var suggestions = xmlhttp.responseText.split(",");
			var li;
			
			// reset li elements
			ul_element.getElementsByTagName("li").item(0).style.color = "#555555";
			for (var i=0; i<5; i++) {
				li = ul_element.getElementsByTagName("li").item(i);
				li.textContent = "";
				li.style.display = "none";
			};
			
			if (suggestions.length == 1 && suggestions[0] == "") {
				li = ul_element.getElementsByTagName("li").item(0);
				li.textContent = "Καμμία πρόταση";
				li.style.color = "#FF4D4D";
				li.style.display = "block";
			}
			else {
				var bound = (suggestions.length < 5) ? suggestions.length : 5;
				for (var i=0; i<bound; i++) {
					li = ul_element.getElementsByTagName("li").item(i);
					li.textContent = suggestions[i];
					li.style.display = "block";
				};
			}
			ul_element.style.display = "block";
		}
	}
	
	xmlhttp.open("GET", "http://localhost/it-project/ajax/autocomplete?q="+input, true);
	xmlhttp.send();
}

function selectTag(num, suggestion)
{
    document.getElementById("suggestions"+num).parentNode.getElementsByTagName("input")[0].value = suggestion;
    document.getElementById("suggestions"+num).style.display = "none";
}

function hideSuggestions(num)
{
    document.getElementById("suggestions"+num).style.display = "none";
}
