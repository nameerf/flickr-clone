function validateUserInputMassUpload(form)
{
	var zipped_images_filename;
	try {
		zipped_images_filename = form["uploaded_zipped_file"].value;
	} catch (err) {
		zipped_images_filename = null;
	}
	
	var valid_extensions = /(.zip)$/i;
	
	var index = 0;
	var error_strings = new Array();
	
	if(zipped_images_filename != null) {
		if(zipped_images_filename != "")
		{
			if(!valid_extensions.test(zipped_images_filename))
				error_strings[index++] = "Το συμπιεσμένο αρχείο πρέπει απαραίτητα να είναι σε μορφή ZIP.";
		}
		else
			error_strings[index++] = "Δεν έχετε επιλέξει κάποιο (συμπιεσμένο) αρχείο για ανέβασμα.";
	}

	if(error_strings.length == 0) {
		document.getElementById("content_bottom").innerHTML = "<p>Παρακαλώ περιμένετε μέχρι να ολοκληρωθεί η διαδικασία "+
															  "ανεβάσματος των φωτογραφιών σας στο προσωπικό σας άλμπουμ. "+
															  "Μόλις ολοκληρωθεί η διαδικασία θα μεταφερθείτε αυτόματα στον "+
															  "προσωπικό σας χώρο.</p>";
		document.body.style.cursor = 'wait';
		return true;
	}

	var error = document.getElementById("content_bottom");

	var error_message = "<p>Τα στοιχεία που δώσατε περιέχουν λάθη. Παρακαλώ διωρθώστε τα και ξαναπροσπαθήστε.</p><br />";
	error_message += "<ul>";
	for( i = 0; i < error_strings.length; i++) {
		if(error_strings[i] == null || error_strings[i] == undefined)
			continue;
		error_message += "<li>" + error_strings[i] + "</li>";
	}
	error_message += "</ul>";

	error.innerHTML = error_message;

	return false;
}

function validateUserInput(form)
{
	var image_filename;
	try {
		image_filename = form["uploaded_image"].value;
	} catch (err) {
		image_filename = null;
	}
	
	var title = form["title"].value;
	var description = form["description"].value;
	var tag1 = form["tag1"].value;
	
	var valid_extensions = /(.png|.jpg|.jpeg)$/i;
	
	var index = 0;
	var error_strings = new Array();
	
	if((image_filename != null) && (image_filename != "edit")) {
		if(image_filename != "")
		{
			if(!valid_extensions.test(image_filename))
				error_strings[index++] = "Η φωτογραφία πρέπει απαραίτητα να είναι σε μορφή PNG ή JPEG.";
		}
		else
			error_strings[index++] = "Δεν έχετε επιλέξει κάποια φωτογραφία για ανέβασμα.";
	}
		
	if(isEmpty(title))
		error_strings[index++] = "Πρέπει να εισάγετε έναν σύντομο τίτλο που να χαρακτηρίζει την φωτογραφία.";
	if(isEmpty(description))
		error_strings[index++] = "Πρέπει να εισάγετε μια σύντομη περιγραφή για την φωτογραφία.";
	if(isEmpty(tag1))
		error_strings[index++] = "Πρέπει να εισάγετε τουλάχιστον μια ετικέτα που να χαρακτηρίζει την φωτογραφία.";

	if(error_strings.length == 0) {
		return true;
	}

	var error = document.getElementById("content_bottom");

	var error_message = "<p>Τα στοιχεία που δώσατε περιέχουν λάθη. Παρακαλώ διωρθώστε τα και ξαναπροσπαθήστε.</p><br />";
	error_message += "<ul>";
	for( i = 0; i < error_strings.length; i++) {
		if(error_strings[i] == null || error_strings[i] == undefined)
			continue;
		error_message += "<li>" + error_strings[i] + "</li>";
	}
	error_message += "</ul>";

	error.innerHTML = error_message;

	return false;
}

function isEmpty(elem_value)
{
	return empty = (elem_value.length == 0) ? true : false;
}
