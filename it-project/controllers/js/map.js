
function getUserLocation()
{
	var mapholder = document.getElementById("mapholder");
    // getLocation
    if (navigator.geolocation) {
    	default_lat = null;
    	default_lng = null;
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    }
    else {
        mapholder.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position)
{
	var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    	
    var latlng = new google.maps.LatLng(lat, lng);
    var mapholder = document.getElementById('mapholder');
    mapholder.style.width = '480px';
    mapholder.style.height = '440px';
    
    var myOptions = {
        center:latlng,zoom:13,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        streetViewControl:false,
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    };
    
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);

	google.maps.event.addListener(map, 'bounds_changed', function() {
    	// AJAX call
    	showNeighboringPhotoMarkers(map, lat, lng);
	});
}

function showError(error)
{
	var mapholder = document.getElementById("mapholder");
	
    switch(error.code) 
    {
        case error.PERMISSION_DENIED:
            mapholder.innerHTML = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            mapholder.innerHTML = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            mapholder.innerHTML = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            mapholder.innerHTML = "An unknown error occurred.";
            break;
    }
}

function showPhotoLocation(map_container, photo_address, photo_lat, photo_lng)
{
    var photo_latlng = new google.maps.LatLng(photo_lat, photo_lng);
    var mapholder = document.getElementById(map_container);
    mapholder.style.width = '290px';
    mapholder.style.height = '140px';
    
    var myOptions = {
        center:photo_latlng,
        zoom:5,
        zoomControl:false,
        draggable:false,
        streetViewControl:false,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false
    };
    
    var map = new google.maps.Map(document.getElementById(map_container), myOptions);
    var marker = new google.maps.Marker({position:photo_latlng,map:map,cursor:"default",title:photo_address});
    
	google.maps.event.addListener(map, 'mouseover', function() {
		map.setZoom(10);
	});
	
	google.maps.event.addListener(map, 'mouseout', function() {
		map.setZoom(5);
	});
	
	google.maps.event.addListener(marker, 'mouseover', function() {
		map.setZoom(15);
	});
	
	google.maps.event.addListener(marker, 'mouseout', function() {
		map.setZoom(10);
	});
}

function showPhotoMarker(map, photo_id, sq_photo_path, th_photo_path, photo_title, photo_address, photo_latlng)
{
    var image = new google.maps.MarkerImage(
        sq_photo_path,
        null,
        null,
        new google.maps.Point(16,16),
        new google.maps.Size(32,32)
    );
    
    var shape = {
        coord: [0,0,32,32],
        type: 'rect'
    };
    
    var photo_marker = new google.maps.Marker({
        icon:image,
        shape:shape,
        map:map,
        position:photo_latlng
    });

    var photoInfoWindow = new google.maps.InfoWindow();
    
    var htmlContentString = "<div style='text-align:center;'>"
                                +"<a href='http://localhost/it-project/photos/view?pid="+photo_id+"'>"
                                    +"<img src='"+th_photo_path+"' />"
                                +"</a>"
                                +"<p style='padding-top:5px; color:#05A4D1;'>"+photo_title+"</p>"
                                +"<p style='padding-top:5px; text-align:left;'>Τοποθεσία: "+photo_address+"</p>"
                            +"</div>";
    
    photoInfoWindow.setContent(htmlContentString);
    
    google.maps.event.addListener(photo_marker, "click", function(event) {
        photoInfoWindow.open(map, this);
    });
}

function showNeighboringPhotoMarkers(map, latitude, longitude)
{
	var xmlhttp;
	
	var map_coordinates = new Array;
	map_coordinates[0] = map.getBounds().getSouthWest().lat();
	map_coordinates[1] = map.getBounds().getSouthWest().lng();
	map_coordinates[2] = map.getBounds().getNorthEast().lat();
	map_coordinates[3] = map.getBounds().getNorthEast().lng();
	
	if (window.XMLHttpRequest) // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	else // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			photos_data_string = xmlhttp.responseText;
			photos_data = JSON.parse(photos_data_string);
			
			for (var i=0; i<photos_data.length; i++) {
				var lat = parseFloat(photos_data[i].latitude);
				var lng = parseFloat(photos_data[i].longitude);
			    var photo_latlng = new google.maps.LatLng(lat, lng);
			    
			    if (map.getBounds().contains(photo_latlng)) {
			    	var photo_pid = photos_data[i].pid;
					var path_parts = photos_data[i].path.split("/");
					var sq_photo_path = path_parts[0]+"/"+path_parts[1]+"/square/sq_"+path_parts[2];
					var th_photo_path = path_parts[0]+"/"+path_parts[1]+"/thumb/th_"+path_parts[2];
					var photo_title = photos_data[i].title;
					var photo_address = photos_data[i].address;
					var photo_address_parts = photo_address.split("+");
					photo_address = photo_address_parts[1]+" "+photo_address_parts[0]+", "+photo_address_parts[2];
			    	showPhotoMarker(map, photo_pid, sq_photo_path, th_photo_path, photo_title, photo_address, photo_latlng);
			    }
			}
		}
	}
	
	xmlhttp.open("GET", "http://localhost/it-project/ajax/mapphotos?q="+map_coordinates, true);
	xmlhttp.send();
}
