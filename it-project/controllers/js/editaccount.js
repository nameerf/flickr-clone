function validateUserInput(form)
{
	var password = form["password"].value;
	var password2 = form["password2"].value;
	var email = form["email"].value;

	var index = 0;
	var error_strings = new Array();

	if(isEmpty(password) && isEmpty(password2) && isEmpty(email)) {
		error_strings[index++] = "Δεν έχετε συμπληρώσει κανένα στοιχείο προς αλλαγή.";
	}
	if(!isEmpty(password) && !isValidLength(password)) {
		error_strings[index++] = "Ο κωδικός πρέπει να είναι μεταξύ 5 και 10 χαρακτήρες.";
	}
	if(!isEmpty(password) && !passwordContainsValidCharacters(password)) {
		error_strings[index++] = "Ο κωδικός πρόσβασης περιέχει μη έγκυρους χαρακτήρες.";
	}
	if(password != password2 != "") {
		error_strings[index++] = "Ο κωδικός δεν επιβεβαιώθηκε.";
	}
	if(!isEmpty(email) && !isValidEmail(email)) {
		error_strings[index++] = "Το e-mail δεν είναι έγκυρο.";
	}

	if(error_strings.length == 0) {
		return true;
	}

	var error = document.getElementById("content_bottom");

	var error_message = "<p>Τα στοιχεία που δώσατε περιέχουν λάθη. Παρακαλώ διωρθώστε τα και ξαναπροσπαθήστε.</p><br />";
	error_message += "<ul>";
	for(i=0; i<error_strings.length; i++) {
		if(error_strings[i] === undefined) {
			continue;
		}
		error_message += "<li>" + error_strings[i] + "</li>";
	}
	error_message += "</ul>";

	error.innerHTML = error_message;

	return false;
}

function isEmpty(elem_value)
{
	return empty = (elem_value.length == 0) ? true : false;
}

function isValidLength(elem_value)
{
	return validLenght = (elem_value.length > 4 && elem_value.length < 11) ? true : false;
}

// this to avoid UTF-8 characters
function passwordContainsValidCharacters(elem_value)
{
	var validPassCharsExp = /^[0-9a-zA-Z\ \`\~\!\@\#\$\%\^\&\*\(\)\_\+\-\=\[\]\{\}\;\"\'\,\.\<\>\?]+$/;
	return alphanumeric = (elem_value.match(validPassCharsExp)) ? true : false;
}

function isValidEmail(elem_value)
{
	var validEmailExp = /^[a-zA-Z0-9\._\-]+@([a-zA-Z0-9][a-zA-Z0-9\-]*\.)+[a-zA-Z]+$/;
	return validEmail = (elem_value.match(validEmailExp)) ? true : false;
}
