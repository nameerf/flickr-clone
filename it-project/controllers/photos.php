<?php

class Photos extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        header('location: '.BASE_URL);
    }

    function view()
    {
        $this->view->photo_id = $_GET["pid"];
        
        $photo = $this->model->fetchFromPhoto($this->view->photo_id, "*");
        
        $this->view->photo_priv = $photo['privateness'];
        $this->view->photo_owner = $this->model->fetchPhotoOwner($this->view->photo_id);
        
        if ($this->view->photo_priv == "private") {
            Session::init();
            if (!Session::get('logged_in') || (Session::get('logged_in') && Session::get('user') != $this->view->photo_owner)) {
                header('location: '.BASE_URL.'error/accessdenied');
            }
        }
        
        $this->view->photo_url = $photo['path'];
        $this->view->photo_title = stripslashes($photo['title']);
        $this->view->photo_desc = stripslashes($photo['description']);
        $this->view->photo_width = $photo['width'];
        $this->view->photo_height = $photo['height'];
        $this->view->photo_address = $photo['address'];
        $this->view->photo_lat = $photo['latitude'];
        $this->view->photo_lon = $photo['longitude'];
        $this->view->photo_views = $photo['views'] + 1;
        $this->view->photo_tags = $this->model->fetchPhotoTags($this->view->photo_id);
        
        $this->view->comments = $this->model->fetchUsersCommentsOnPhoto($this->view->photo_id);
        
        $this->model->updatePhotoViews($this->view->photo_id, $photo['views']);
        
        $this->view->style = "view";
        $this->view->render('photos/view');
    }

    function viewFullSize()
    {
        $this->view->photo_id = $_GET["pid"];
        
        $photo = $this->model->fetchFromPhoto($this->view->photo_id, "privateness, width");
        
        $photo_priv = $photo['privateness'];
        $photo_owner = $this->model->fetchPhotoOwner($this->view->photo_id);
        
        if ($photo_priv == "private") {
            Session::init();
            if (!Session::get('logged_in') || (Session::get('logged_in') && Session::get('user') != $photo_owner)) {
                header('location: '.BASE_URL.'error/accessdenied');
            }
        }
        
        $this->view->photo_width = $photo['width'];
        
        $this->view->style = "main";
        $this->view->render('photos/viewfullsize');
    }

    function addComment()
    {
        Session::init();
        $logged_in = Session::get('logged_in');
        if ($logged_in == false) {
            Session::destroy();
            header('location: '.BASE_URL.'error/authorizationrequired/comment');
        }
        
        $pid = $_POST["photo_id"];
        $comment = $_POST["comment"];
        
        $this->model->saveUserComment($pid, $comment);
        
        $current_page_url = str_replace(ROOT_DIR_NAME, "", $_POST['current_url']);
        header('location: '.BASE_URL.$current_page_url);
    }

}
