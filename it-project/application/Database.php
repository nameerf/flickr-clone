<?php

class Database extends PDO
{
    
    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {
        parent::__construct($DB_TYPE.':host='.$DB_HOST.';dbname='.$DB_NAME, $DB_USER, $DB_PASS);
        //parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTIONS);
    }
    
    public function select($sql, $array = array(), $arrayDim = 1, $fetchHowMany = "all", $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        if ($arrayDim == 1) {
            foreach ($array as $key => $value) {
                $sth->bindValue("$key", $value);
            }
        }
        else {
            foreach ($array as $key => $values) {
                $sth->bindValue("$key", $values[0], $values[1]);
            }
        }
        
        $sth->execute();
        
        $data = ($fetchHowMany == "one") ? $sth->fetch($fetchMode) : $sth->fetchAll($fetchMode);
		return $data;
    }
    
    public function insert($sql, $array, $arrayDim = 1)
    {
        ksort($array);
		
        $sth = $this->prepare($sql);
        if ($arrayDim == 1) {
            foreach ($array as $key => $value) {
                $sth->bindValue("$key", $value);
            }
        }
        else {
            foreach ($array as $key => $values) {
                $sth->bindValue("$key", $values[0], $values[1]);
            }
        }
        
        $sth->execute();
    }
    
    public function update($sql, $array)
    {
        ksort($array);
        
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        
        $sth->execute();
    }
    
    public function delete($sql)
    {
        return $this->exec($sql);
    }

}
