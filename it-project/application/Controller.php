<?php

class Controller
{
    
    function __construct()
    {
        $this->view = new View();
    }

    function loadModel($name)
    {
        $file = 'models/' . $name . '_model.php';
        
        if (file_exists($file)) {            
            require 'models/' . $name . '_model.php';
            
            $modelName = $name . 'Model';
            $this->model = new $modelName();
        }
    }

}
