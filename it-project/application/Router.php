<?php

class Router
{

    function __construct()
    {
        if (!isset($_GET['url'])) {
            $_GET['url'] = "index";
        }

        $url = rtrim($_GET['url'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        
        $file = 'controllers/' . $url[0] . '.php';
        if (file_exists($file)) {
            require $file;
        }
        else {
            header('location: '.BASE_URL.'error/pagenotfound');
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);

        // calling methods
        if (isset($url[2])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]);
            }
            else {
                header('location: '.BASE_URL.'error/pagenotfound');
            }
        }
        else {
            if (isset($url[1])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                }
                else {
                    header('location: '.BASE_URL.'error/pagenotfound');
                }
            }
            else {
                $controller->index();
            }
        }
    }

}
