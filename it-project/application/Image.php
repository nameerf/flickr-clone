<?php

class Image
{

    private $image;
    private $type;
    private $width;
    private $height;
    private $ratio;

    public function __construct($filename)
    {
        /** Make sure the file exists so we don't waste time */
        if (!file_exists($filename)) {
            $this->error = "This file doesn't exist";
            return false;
        }
        $image_info = getimagesize($filename);
        $this->width = $image_info[0];
        $this->height = $image_info[1];
        $this->type = $image_info[2];
        $this->ratio = $this->width / $this->height;
        
        if ($this->type == IMAGETYPE_JPEG)
            $this->image = imagecreatefromjpeg($filename);
        elseif ($this->type == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
            imagealphablending($this->image, false); // setting alpha blending on
            imagesavealpha($this->image, true); // save alphablending setting (important)
        }
    }

    public function needResize($max_width, $max_height)
    {
        if ($this->width > $max_width || $this->height > $max_height)
            return true;
        else
            return false;
    }

    public function calculateNewSize($max_width, $max_height)
    {
        $new_width;
        $new_height;
        
        if ($this->width >= $this->height) {
            $new_width = $max_width;
            $new_height = $max_width / $this->ratio;
        }
        else {
            $new_width = $max_width * $this->ratio;
            $new_height = $max_width;
        }
        
        return array($new_width, $new_height);
    }
    
    public function resize($new_width, $new_height, $width, $height)
    {
        $new_image = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $this->image = $new_image;
    }
    
    public function cropAndResize($x, $y, $new_width, $new_height, $width, $height)
    {
        $new_image = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($new_image, $this->image, 0, 0, $x, $y, $new_width, $new_height, $width, $height);
        $this->image = $new_image;
    }
    
    public function save($filename, $type = IMAGETYPE_JPEG, $quality = 75, $permissions = null)
    {
        if ($type == IMAGETYPE_JPEG)
            imagejpeg($this->image, $filename, $quality);
        elseif ($type == IMAGETYPE_PNG)
            imagepng($this->image, $filename);
        
        if ($permissions != null)
            chmod($filename, $permissions);
            
        /* Remove image from memory */
        @imagedestroy($this->image);
    }

    public function output($type = IMAGETYPE_JPEG)
    {
        if ($type == IMAGETYPE_JPEG)
            imagejpeg($this->image);
        elseif ($type == IMAGETYPE_PNG)
            imagepng($this->image);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight() 
    {
        return $this->height;
    }
    
    public function getRatio() 
    {
        return $this->ratio;
    }

}
