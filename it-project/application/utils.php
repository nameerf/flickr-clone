<?php

function addressToCoordinates($address)
{
    $bad = array(" " => "+", "," => "", "?" => "", "&" => "", "=" => "");
    
    $address = str_replace(array_keys($bad), array_values($bad), $address);
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$address}&sensor=false";
    
    $source = file_get_contents($url);
    $obj = json_decode($source);
    
    $latitude = $obj->results[0]->geometry->location->lat;
    $longitude = $obj->results[0]->geometry->location->lng;
    
    return array("latitude" => $latitude, "longitude" => $longitude);
}

// recursively remove a directory
function rrmdir($dir)
{
    foreach(glob($dir . '/*') as $file) {
        if(is_dir($file))
            rrmdir($file);
        else
            unlink($file);
    }
    rmdir($dir);
}

?>
