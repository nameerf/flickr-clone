<?php

require 'config.php';

require 'application/utils.php';

require 'models/photo_model.php';

// autoloader
function __autoload($class) {
    require APP_DIR_NAME . $class .".php";
}

$app = new Router();
